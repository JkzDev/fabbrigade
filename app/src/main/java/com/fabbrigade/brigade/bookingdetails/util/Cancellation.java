package com.fabbrigade.brigade.bookingdetails.util;

import android.content.Context;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.booking.model.Booking;

import java.util.Calendar;

/**
 * Created by Laptop on 11/13/2015.
 */
public class Cancellation {

    private static final long BOOKING_GRACE_PERIOD_MILLIS = 5000;
    private static final long BOOKING_APPT_TIME_FAR = 3 * 3600 * 1000; // 3 hours
    private static final long BOOKING_APPT_TIME_NEAR = 3600 * 1000; // 1 hour

    private static final String BOOKING_FAR_PERCENT = "100%";
    private static final String BOOKING_MID_PERCENT = "50%";

    public static String getCancellationMessage(Context ctx, Booking booking) {
        long now = Calendar.getInstance().getTimeInMillis();
        long bookingCreation = booking.getCreatedAt().getTime();
        long bookingApptTime = booking.getDate().getTime();

        if (now - bookingCreation < BOOKING_GRACE_PERIOD_MILLIS) {
            return ctx.getString(R.string.cancellation_grace_period);
        }

        long timeLeftUntilAppt = bookingApptTime - now;
        if  (timeLeftUntilAppt > BOOKING_APPT_TIME_FAR) {
            return ctx.getString(R.string.cancellation_far, BOOKING_FAR_PERCENT);
        } else if (timeLeftUntilAppt > BOOKING_APPT_TIME_NEAR) {
            return ctx.getString(R.string.cancellation_far, BOOKING_MID_PERCENT);
        } else {
            return ctx.getString(R.string.cancellation_near);
        }
    }
}

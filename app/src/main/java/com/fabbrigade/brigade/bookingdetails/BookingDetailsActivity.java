package com.fabbrigade.brigade.bookingdetails;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.shared.BaseActivity;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by Laptop on 10/27/2015
 */
public class BookingDetailsActivity extends BaseActivity {

    public static final String ARG_BOOKING_ID = "booking_id";
    protected Booking booking;

    private static final String DATE_FORMAT = "MMM d yyyy";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_toolbar);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String bookingId = getIntent().getStringExtra(ARG_BOOKING_ID);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Booking");
        query.whereMatches("objectId", bookingId);

        query.fromLocalDatastore();
        query.getInBackground(bookingId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e != null) {
                    Timber.e(e, "Failed to get Booking");
                    return;
                }else{



                    Utils.booking = (Booking) object;
                    booking = (Booking) object;
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.container, new BookingDetailsFragment())
                            .commit();

                }



//                List<Service> services = booking.getServices();
//                Service service = services.get(0);
//                Category category = service.getCategory();
//                Category parent = category.getParent();
//                String categoryName = parent.getName();
//                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
//
//                String title = getString(R.string.booking_details_toolbar_title, categoryName, simpleDateFormat.format(booking.getDate()));

                final ActionBar actionBar = getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setDisplayShowHomeEnabled(true);
                    actionBar.setDisplayHomeAsUpEnabled(true);
                    actionBar.setTitle("My booking");
                }




            }
        });



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }






}

package com.fabbrigade.brigade.bookingdetails;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.bookingdetails.util.Cancellation;
import com.fabbrigade.brigade.rating.model.Rating;
import com.fabbrigade.brigade.service.model.Service;
import com.fabbrigade.brigade.shared.BaseFragment;
import com.fabbrigade.brigade.shared.model.FabUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Laptop on 10/27/2015.
 */
public class BookingDetailsFragment extends BaseFragment implements View.OnClickListener {
//public class BookingDetailsFragment extends BaseFragment{

    private Booking booking;

    @Bind(R.id.booking_detail_address1) TextView address1;
    @Bind(R.id.booking_detail_address2) TextView address2;
    @Bind(R.id.booking_detail_date1) TextView date1;
    @Bind(R.id.booking_detail_date2) TextView date2;
    @Bind(R.id.booking_detail_services) TextView services;
    @Bind(R.id.booking_detail_cost) TextView cost;
    @Bind(R.id.booking_detail_payment) TextView payment;
    @Bind(R.id.booking_detail_booking_ref) TextView bookingRef;
    @Bind(R.id.booking_detail_brigadette) TextView brigadette;
    @Bind(R.id.booking_detail_brigadette_block) LinearLayout brigadetteBlock;
    @Bind(R.id.booking_detail_status) TextView status;
    @Bind(R.id.booking_details_rate_bar) RatingBar ratingBar;
    @Bind(R.id.booking_details_confirmation_description) TextView confirmationDescription;
    @Bind(R.id.booking_details_image0) ImageView serviceImage0;
    @Bind(R.id.booking_details_image1) ImageView serviceImage1;
    @Bind(R.id.booking_details_image2) ImageView serviceImage2;
    @Bind(R.id.booking_details_image3) ImageView serviceImage3;
    @Bind(R.id.booking_details_image4) ImageView serviceImage4;

    @Bind(R.id.booking_detail_cancel_block) LinearLayout cancelBlock;
    @Bind(R.id.booking_detail_cancel_booking) TextView cancelBooking;

    @Bind(R.id.service_block)
    RelativeLayout servicesBlock;
    @Bind(R.id.booking_details_widget_block) LinearLayout serviceDetailsBlock;
    @Bind(R.id.booking_details_service_expand_button) ImageView serviceExpandButton;

    private boolean SERVICES_EXPANDED = false;

    private static final String DATE1_FORMAT = "EEEE, MMMM d";
    private static final String DATE2_FORMAT = "h:mma";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_booking_details, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Activity activity = getActivity();
        if (activity instanceof BookingDetailsActivity) {
            booking = ((BookingDetailsActivity)activity).booking;


                setViewsText();
                setViewsBasedOnStatus();





        }
        servicesBlock.setOnClickListener(this);
        cancelBooking.setOnClickListener(this);

    }
    public void setViewsText() {
        if(booking.getOmiseChargeId()!=null){

            String paymentId = booking.getOmiseChargeId();
            if (!paymentId.toString().equalsIgnoreCase("")||paymentId.equalsIgnoreCase(null) ){
                payment.setText(paymentId);
            }

        }



        String ref = booking.getObjectId();
        if (ref != null) {
            bookingRef.setText(ref);
        }

        address1.setText(booking.getMailingAddressLine1());
        address2.setText(booking.getMailingAddressLine2());

        List<Service> serviceList = booking.getServices();
        double price = 0;
        String serviceString = "";
        int count = 0;
        long duration = 0;
        for (Service service : serviceList) {
            try {
                duration = duration +  service.getDuration();

            price = price + service.getPrice();
            setImage(count, service);
            if (count == 0) {
                serviceString = service.getName();

            } else {
                serviceString = serviceString + ",\n" + service.getName();
            }
            }catch (Exception e){
                e.printStackTrace();
            }
            count++;
        }
        cost.setText(Double.toString(price));
        services.setText(serviceString);

        booking.setStatusImage(getActivity(), status);

        Date startDate = booking.getDate();
        Date endDate = new Date(startDate.getTime() + (duration * 60 * 1000));
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(DATE1_FORMAT, Locale.getDefault());
        date1.setText(simpleDateFormat1.format(startDate));

        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(DATE2_FORMAT, Locale.getDefault());
        date2.setText(getString(R.string.booking_details_date_split, simpleDateFormat2.format(startDate), simpleDateFormat2.format(endDate)));

        FabUser bookingBrigadette = booking.getBrigadette();
        if (bookingBrigadette != null) {
            brigadette.setText(bookingBrigadette.getFirstName(getActivity()));
        }

        Rating rating = booking.getRating();
        if (rating != null) {
            ratingBar.setProgress(rating.getStarsInt());
        }

    }
    private void setImage(int position, Service service) {
        ImageView imageView = null;
        switch (position) {
            case 0:
                imageView = serviceImage0;
                break;
            case 1:
                imageView = serviceImage1;
                break;
            case 2:
                imageView = serviceImage2;
                break;
            case 3:
                imageView = serviceImage3;
                break;
            case 4:
                imageView = serviceImage4;
                break;
        }
        if (imageView != null) {
            imageView.setVisibility(View.VISIBLE);
            Glide.with(getActivity())
                    .load(service.getImageUrl())
                    .centerCrop()
                    .into(imageView);
        }

    }

    public void setViewsBasedOnStatus() {
        switch (booking.getStatus()) {
            case Booking.STATUS_OPEN:
                brigadetteBlock.setVisibility(View.GONE);
                ratingBar.setVisibility(View.GONE);
                break;
            case Booking.STATUS_CONFIRMED:
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                break;
            case Booking.STATUS_STARTED:
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                break;
            case Booking.STATUS_ENDED:
                confirmationDescription.setVisibility(View.GONE);
                cancelBlock.setVisibility(View.GONE);
                break;
            case Booking.STATUS_CANCELLED_BRIGADETTE:
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                cancelBlock.setVisibility(View.GONE);
                break;
            case Booking.STATUS_CANCELLED_CUSTOMER:
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                cancelBlock.setVisibility(View.GONE);
                break;
            case Booking.STATUS_NOMATCH:
                brigadetteBlock.setVisibility(View.GONE);
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                cancelBlock.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        if (v == servicesBlock) {

            int rotateAnimationInt = SERVICES_EXPANDED ? R.anim.rotate_out : R.anim.rotate_in;
            Animation rotate = AnimationUtils.loadAnimation(getActivity(), rotateAnimationInt);
            rotate.setFillAfter(true);

            rotate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    int index = serviceDetailsBlock.indexOfChild(servicesBlock);

                    if (SERVICES_EXPANDED) {
                        animateOut(index);

                    } else {
                        List<Service> services = booking.getServices();
                        for (Service service : services) {
                            insertView(service, index);
                            index++;
                        }
                    }
                    SERVICES_EXPANDED = !SERVICES_EXPANDED;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            serviceExpandButton.startAnimation(rotate);

        } else if (v == cancelBooking) {
            String cancellationMessageId = Cancellation.getCancellationMessage(getActivity(), booking);
            displayCancellationMessage(cancellationMessageId);
        }
    }

    private void displayCancellationMessage(String cancellationMessageId) {
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.booking_detail_cancel_policy)
                .setMessage(cancellationMessageId)
                .setNegativeButton(R.string.booking_details_do_not_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })

                .setPositiveButton(R.string.booking_details_confirm_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        booking.setStatus(Booking.STATUS_CANCELLED_CUSTOMER);
                        booking.saveInBackground();
                        setViewsText();
                        setViewsBasedOnStatus();
                    }
                })
                .create();
        Button b = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        if(b != null) {
            b.setTextColor(getResources().getColor(R.color.black));
        }

        dialog.show();

    }
    private void animateOut(final int index) {
        for (int i = 1; i < booking.getServices().size() + 1; i++) {
            final View view = serviceDetailsBlock.getChildAt(index + i);
            Animation removeAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_to_right);
            removeAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }
                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                }
                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            view.startAnimation(removeAnimation);
        }
    }
    private void insertView(Service service, int index) {
        View layout2 = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_booking_details_service, serviceDetailsBlock, false);
        ImageView imageView = (ImageView) layout2.findViewById(R.id.item_service_image);
        TextView text1 = (TextView) layout2.findViewById(R.id.item_service_text1);
        TextView text2 = (TextView) layout2.findViewById(R.id.item_service_text2);

        text1.setText(service.getName());
        String price = String.valueOf((int)service.getPrice());
        long duration = service.getDuration();
        if (duration < 60) {
            text2.setText(getActivity().getString(
                    R.string.booking_details_service_summary_no_hours,
                    String.valueOf(duration), price));
        } else {
            long hours = duration / 60;
            long minutes = duration - (60*hours);

            if (minutes == 0) {
                text2.setText(getActivity().getString(
                        R.string.booking_details_service_summary_no_minutes,
                        String.valueOf(hours), price));
            } else {
                text2.setText(getActivity().getString(
                        R.string.booking_details_service_summary,
                        String.valueOf(hours), String.valueOf(minutes), price));
            }
        }

        Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_from_right);
        layout2.startAnimation(animation);
        serviceDetailsBlock.addView(layout2, ++index);
        Glide.with(getActivity())
                .load(service.getImageUrl())
                .centerCrop()
                .into(imageView);
    }
}

package com.fabbrigade.brigade.walkthrough;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.shared.BaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Laptop on 11/1/2015.
 */
public class WalkthroughFragment extends BaseFragment implements View.OnClickListener {

    private static final String ARG_POSITION = "position_num";

    @Bind(R.id.walkthrough_description)
    TextView description;
    @Bind(R.id.walkthrough_title)
    TextView title;
    @Bind(R.id.walkthrough_title_image)
    ImageView logo;
    @Bind(R.id.walkthrough_button_finish)
    Button finishButton;
    @Bind(R.id.last_tutor)
    LinearLayout lastTutor;
    @Bind(R.id.image_tutor)
    ImageView imageTutor;

    public static WalkthroughFragment getInstance(int position) {
        final Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        final WalkthroughFragment fragment = new WalkthroughFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_walkthrough, container, false);
        ButterKnife.bind(this, view);
        finishButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int position = getArguments().getInt(ARG_POSITION);
        int descriptionString = 0;
        int titleString = 0;
        lastTutor.setVisibility(View.GONE);



        switch (position) {
            case 0:
                descriptionString = R.string.walkthrough_description0;
                imageTutor.setImageResource(R.drawable.step1);
                break;
            case 1:
                titleString = R.string.walkthrough_title1;
                descriptionString = R.string.walkthrough_description1;
                imageTutor.setImageResource(R.drawable.step2);
                logo.setVisibility(View.GONE);
                break;
            case 2:
                titleString = R.string.walkthrough_title2;
                descriptionString = R.string.walkthrough_description2;
                logo.setVisibility(View.GONE);
                imageTutor.setImageResource(R.drawable.step3);
                finishButton.setVisibility(View.VISIBLE);
                break;
        }
        if (descriptionString != 0) {
            description.setText(descriptionString);
        }
        if (titleString != 0) {
            title.setVisibility(View.VISIBLE);
            title.setText(getString(titleString));
        }


    }

    @Override
    public void onClick(View v) {
        if (v == finishButton) {
            WalkthroughActivity.tutorialFinished(getActivity());
            getActivity().finish();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}

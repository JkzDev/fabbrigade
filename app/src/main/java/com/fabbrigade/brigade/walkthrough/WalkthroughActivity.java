package com.fabbrigade.brigade.walkthrough;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.shared.BaseActivity;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Laptop on 11/1/2015.
 */
public class WalkthroughActivity extends BaseActivity implements View.OnClickListener {

    private static final int NUM_PAGES = 3;
    private PagerAdapter mPagerAdapter;

    public static final String TUTORIAL_FINISHED = "tutorial_finished";

    @Bind(R.id.pager) ViewPager mPager;
    @Bind(R.id.walkthrough_close) ImageView closeButton;
    @Bind(R.id.pager_indicator) CirclePageIndicator pagerIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthrough);
        ButterKnife.bind(this);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        pagerIndicator.setViewPager(mPager);

        closeButton.setOnClickListener(this);

    }
    public static void tutorialFinished(Context ctx) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        edit.putBoolean(TUTORIAL_FINISHED, true);
        edit.apply();
    }
    public void skipToNextPage() {
        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            tutorialFinished(this);
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == closeButton) {
            tutorialFinished(WalkthroughActivity.this);
            finish();
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return WalkthroughFragment.getInstance(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}

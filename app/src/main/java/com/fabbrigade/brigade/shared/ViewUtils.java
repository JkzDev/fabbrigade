package com.fabbrigade.brigade.shared;

import android.app.Activity;
import android.content.res.TypedArray;
import android.util.DisplayMetrics;

/**
 * Copyright FabBrigade 2015
 */
public class ViewUtils {
    public static int getContentHeight(Activity activity, boolean excludeStatusBar, boolean excludeActionBar, boolean excludeNavigationBar) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);

        final TypedArray styledAttributes = activity.getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        final int actionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        int statusBarSize = 0;
        final int statusBarResourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (statusBarResourceId > 0) {
            statusBarSize = activity.getResources().getDimensionPixelSize(statusBarResourceId);
        }

        int navigationBarSize = 0;
        final int navigationBarResourceId = activity.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (navigationBarResourceId > 0) {
            navigationBarSize = activity.getResources().getDimensionPixelSize(navigationBarResourceId);
        }

        return dm.heightPixels - (excludeActionBar ? actionBarSize : 0) - (excludeStatusBar ? statusBarSize : 0) - (excludeNavigationBar ? navigationBarSize : 0);
    }

    public static int getContentHeight(Activity activity) {
        return getContentHeight(activity, true, true, false);
    }

    public static int getContentWidth(Activity activity) {
        final DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }
}

package com.fabbrigade.brigade.shared.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fabbrigade.brigade.service.model.Service;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

/**
 * Created by Laptop on 11/3/2015.
 */
public class Cart {

    private List<Service> services;
    private String categoryId;
    private static final String SAVE_PREF = "cart_";
    private CartChangedListener listener;

    public Cart(String categoryId, Context ctx, CartChangedListener listener) {
        services = new ArrayList<>();
        this.categoryId = categoryId;
        loadCart(ctx, listener);
    }

    public List<Service> getServices() {
        return services;
    }
    public void addServiceToCart(Service service, Context ctx) {

        services.add(service);
        saveCart(ctx);
    }
    public Boolean containsService(Service service) {
        return services.contains(service);
    }

    public void removeServiceFromCart(Service service, Context ctx) {
        services.remove(service);
        saveCart(ctx);
    }
    private void saveCart(Context ctx) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        String key = SAVE_PREF + categoryId;
        if (services.size() > 0) {
            Set<String> set = new HashSet<>();
            for (Service service : services) {
                set.add(service.getObjectId());
            }
            edit.putStringSet(key, set);
        } else {
            edit.remove(key);
        }
        edit.commit();
        listener.onCartChanged();
    }
    public int size() {
        return services.size();
    }
    public ArrayList<String> getServiceIds() {
        ArrayList<String> ids = new ArrayList<>();
        for (Service service : services) {
            ids.add(service.getObjectId());
        }
        return ids;
    }
    public void emptyCart(Context ctx) {
        services.clear();
        saveCart(ctx);
    }
    private void loadCart(final Context ctx, final CartChangedListener cartChangedListener) {
        listener = cartChangedListener;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        Set<String> set = prefs.getStringSet(SAVE_PREF + categoryId, new HashSet<String>());

        if (set.size() > 0) {

            List<String> serviceIds = new ArrayList<>(set);

            ParseQuery<Service> query = ParseQuery.getQuery("Service");
            query.whereContainedIn("objectId", serviceIds);

            query.fromLocalDatastore();
            query.findInBackground(new FindCallback<Service>() {
                @Override
                public void done(List<Service> objects, ParseException e) {
                    if (e == null) {
                        Timber.d("Loaded " + objects.size() + " services to cart");
                        services.addAll(objects);
                        listener.onCartChanged();
                    } else {
                        Timber.e(e, "Error getting services for cart from local datastore");
                    }
                }
            });
        } else {
            listener.onCartChanged();
        }
    }

    public interface CartChangedListener {
        void onCartChanged();
    }


}

package com.fabbrigade.brigade.shared;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fabbrigade.brigade.R;

import static butterknife.ButterKnife.findById;

/**
 * Copyright FabBrigade 2015
 */
public abstract class BaseRecyclerViewFragment<T extends RecyclerView.Adapter> extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // init recycler view
        final RecyclerView list = findById(view, R.id.booking_list);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(getAdapter());
    }

    protected abstract T getAdapter();
}

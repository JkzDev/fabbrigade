package com.fabbrigade.brigade.shared.model;

import android.content.Context;
import android.os.Bundle;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.parse.ParseClassName;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright FabBrigade 2015
 */

@ParseClassName("_User")
public class FabUser extends ParseUser {
    
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_FACEBOOK_ID = "facebookId";
    private static final String COLUMN_OMISE = "omiseCustomerId";

    public String getFirstName(Context ctx) {
        String fullName = getString(COLUMN_NAME);
        if (fullName == null) {
            fullName = ctx.getString(R.string.anonymous_user);
        }
        String[] partialNames = fullName.split(" ");
        return partialNames[0];
    }
    public String getFullName(Context ctx) {
        String fullName = getString(COLUMN_NAME);
        if (fullName == null) {
            return ctx.getString(R.string.anonymous_user);
        }
        return fullName;
    }
    private String getFullName() {
        return getString(COLUMN_NAME);
    }
    public void setFullName(String name) {
        if (name != null) {
            put(COLUMN_NAME, name);
        }
    }

    public void setEmail(String email) {
        if (email != null) {
            put(COLUMN_EMAIL, email);
        }
    }

    public void setOmiseId(String id) {
        if (id != null) {
            put(COLUMN_OMISE, id);
        }
    }
    public String getOmiseId() {
        return getString(COLUMN_OMISE);
    }
    public void setFacebookId(String id) {
        if (id != null) {
            put(COLUMN_FACEBOOK_ID, id);
        }
    }
    public String getFacebookId() {
        return getString(COLUMN_FACEBOOK_ID);
    }

    public String getEmail(Context ctx) {
        String email = getString(COLUMN_EMAIL);
        if (email == null) {
            return ctx.getString(R.string.anonymous_user_email);
        }
        return email;
    }
    public void getProfileImage() {
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/{user-id}/picture",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
            /* handle the result */
                    }
                }
        ).executeAsync();
    }
    public static void saveFBDetails() {
        Profile profile = Profile.getCurrentProfile();
        FabUser user = (FabUser) ParseUser.getCurrentUser();



        user.setFacebookId(profile.getId());
        user.setFullName(profile.getName());
        user.getFBEmailInBackground();
        user.saveEventually();
    }
    public static void getFBEmailInBackground() {
        FabUser user = (FabUser) ParseUser.getCurrentUser();
        GraphRequest request = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + user.getFacebookId(),
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
    /* handle the result */
                        JSONObject jsonObject = response.getJSONObject();
                        try {
                            String email = jsonObject.getString("email");
                            Utils.Email = email;

                            FabUser user = (FabUser) ParseUser.getCurrentUser();
                            user.setEmail(email);
                            user.saveEventually();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields", "email");
        request.setParameters(parameters);
        request.executeAsync();

    }

}

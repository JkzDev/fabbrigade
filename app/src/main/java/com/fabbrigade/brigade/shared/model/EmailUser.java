package com.fabbrigade.brigade.shared.model;

import com.parse.ParseClassName;
import com.parse.ParseUser;

/**
 * Copyright FabBrigade 2015
 */

@ParseClassName("_User")
public class EmailUser extends ParseUser {
    
    private static final String COLUMN_USER = "username";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_EMAIL = "email";



    public void setName(String name) {
        if (name != null) {
            put(COLUMN_NAME, name);
        }
    }

    public void setUser(String name) {
        if (name != null) {
            put(COLUMN_USER, name);
        }
    }

    public void setEmail(String name) {
        if (name != null) {
            put(COLUMN_EMAIL, name);
        }
    }





    public static void saveFBDetails(String name,String Email,String email) {
//        EmailUser user = new EmailUser("User");
//        user.setName(name);
//        user.setUser(name);
//        user.setEmail(name);
//        user.saveInBackground();

    }


}

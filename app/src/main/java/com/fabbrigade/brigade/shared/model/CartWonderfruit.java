package com.fabbrigade.brigade.shared.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.tabpage.wonderfruitVO;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

/**
 * Created by Laptop on 11/3/2015.
 */
public class CartWonderfruit {

    private ArrayList<wonderfruitVO> wonderfruit;
    private String categoryId;
    private static final String SAVE_PREF = "cart_";
    private CartChangedListener listener;
    ArrayList<wonderfruitVO>  wonderLst;
    ArrayList<wonderfruitVO>  childLst;
    List<String> serviceIds;


    public CartWonderfruit(String categoryId, Context ctx, CartChangedListener listener) {
        wonderfruit = new ArrayList<>();
        this.categoryId = categoryId;
        loadCart(ctx, listener);
    }

    public ArrayList<wonderfruitVO> getServices() {
        return wonderfruit;
    }
    public void addServiceToCart(wonderfruitVO wonderfruitVO, Context ctx) {

        wonderfruit.add(wonderfruitVO);
        saveCart(ctx);
    }
    public Boolean containsService(wonderfruitVO service) {
        return wonderfruit.contains(service);
    }

    public void removeServiceFromCart(wonderfruitVO service, Context ctx) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        Set<String> set = prefs.getStringSet(SAVE_PREF + categoryId, new HashSet<String>());

                    wonderfruit.remove(service);
                    saveCart(ctx);


    }
    private void saveCart(Context ctx) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        String key = SAVE_PREF + categoryId;
        if (wonderfruit.size() > 0) {
            Utils.cart_total = wonderfruit.size();

            Set<String> set = new HashSet<>();
            for (wonderfruitVO service : wonderfruit) {
                set.add(service.getId());
            }
            edit.putStringSet(key, set);
        } else {
            Utils.cart_total = wonderfruit.size();
            edit.remove(key);
        }
        edit.commit();
        listener.onCartChanged();
    }


    public int size() {
        return wonderfruit.size();
    }
    public ArrayList<String> getServiceIds() {
        ArrayList<String> ids = new ArrayList<>();
        for (wonderfruitVO service : wonderfruit) {
            ids.add(service.getId());
        }
        return ids;
    }
    public void emptyCart(Context ctx) {
        wonderfruit.clear();
        saveCart(ctx);
    }
    private void loadCart(final Context ctx, final CartChangedListener cartChangedListener) {
        listener = cartChangedListener;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        Set<String> set = prefs.getStringSet(SAVE_PREF +categoryId, new HashSet<String>());

        if (set.size() > 0) {

           serviceIds = new ArrayList<>(set);
            loadData();


//            ParseQuery<Service> query = ParseQuery.getQuery("Service");
//            query.whereContainedIn("objectId", serviceIds);
//
//            query.fromLocalDatastore();
//            query.findInBackground(new FindCallback<Service>() {
//                @Override
//                public void done(ArrayList<Service> objects, ParseException e) {
//                    if (e == null) {
//                        Timber.d("Loaded " + objects.size() + " services to cart");
//                        wonderfruit.addAll(objects);
//                        listener.onCartChanged();
//                    } else {
//                        Timber.e(e, "Error getting services for cart from local datastore");
//                    }
//                }
//            });
        } else {
            listener.onCartChanged();
        }
    }

    public interface CartChangedListener {
        void onCartChanged();
    }



    public  void loadData(){

        new AsyncTask<Void, Void, String>() {
            JSONArray jsonArray = null;
            String data;

            @Override
            protected String doInBackground(Void... voids) {
                OkHttpClient okHttpClient = new OkHttpClient();

                if(Utils.from_menu == 0){
                    data = Utils.OKHttpPostData("http://jakki3zservice.appspot.com/jakki3zservice");
                }else{
                    data = Utils.OKHttpPostData("http://jakki3zservice.appspot.com/GlamService");
                }


                return data;
            }


            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                try {
                    wonderLst = new ArrayList<>();
                    jsonArray  = new JSONArray(string);
                    for(int i=0;i<jsonArray.length();i++){


                        JSONObject datalist = jsonArray.getJSONObject(i);
                        JSONArray data = datalist.getJSONArray("data");
                        childLst = new ArrayList<>();

                        for(int x=0;x<data.length();x++){
                            JSONObject dataObj = data.getJSONObject(x);


                            for(int t =0;t<serviceIds.size();t++){
                                if(dataObj.getString("Id").contains(serviceIds.get(t))){
                                    childLst.add(new wonderfruitVO(
                                            dataObj.getString("Id"),
                                            dataObj.getString("description"),
                                            dataObj.getString("duration"),
                                            dataObj.getString("image"),
                                            dataObj.getString("image_small"),
                                            dataObj.getString("name"),
                                            dataObj.getString("price"),
                                            dataObj.getString("serviceDescription")
                                    ));
                                }


                            }


                        }

                        wonderLst.add(new wonderfruitVO(
                                datalist.getString("CateId"),
                                datalist.getString("name"),
                                childLst)
                        );

                    }

                    Log.i("size",""+jsonArray.length());

                    Utils.cart_total = childLst.size();

                    Timber.d("Loaded " + childLst.size() + " services to cart");
                    wonderfruit.addAll(childLst);
                    listener.onCartChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }
        }.execute();



    }




}

package com.fabbrigade.brigade.shared;

import android.content.Context;

import org.joda.time.Duration;

/**
 * Copyright FabBrigade 2015
 */
public class StringUtils {
    public static String getCurrencyString(Context context, double value) {
//        return (new DecimalFormat(context.getString(R.string.currency_pattern))).format(value);

      return  value+" THB";

    }

    /**
     * Return a string representation of a duration
     * @param context Context where this method is called
     * @param duration Duration in minutes
     * @return Duration string
     */
    public static String getTimeString(Context context, int duration) {
        Duration d = new Duration(duration * 60000); // convert minutes to milliseconds
        StringBuilder sb = new StringBuilder();

        if (d.getStandardHours() > 0) {
            sb.append(d.getStandardHours() + "HR ");
        }

        if (d.getStandardMinutes() - (d.getStandardHours() * 60) > 0) {
            sb.append(d.getStandardMinutes() - (d.getStandardHours() * 60) + "MIN");
        }

        return sb.toString();
    }
}

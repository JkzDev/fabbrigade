package com.fabbrigade.brigade.shared;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by Laptop on 10/27/2015.
 */
public class QueryUtils {
    public static ParseQuery<ParseObject> addMatchCurrentUserToQuery(ParseQuery<ParseObject> query, Boolean creditCard) {
        ParseUser currentUser = ParseUser.getCurrentUser();
        String objectId = currentUser.getObjectId();
        ParseQuery<ParseUser> innerQuery = ParseQuery.getQuery("_User");
        innerQuery.whereMatches("objectId", objectId);

        String columnName;
        if (creditCard) {
            columnName = "user";
        } else {
            columnName = "client";
        }
        query.include(columnName);
        query.whereMatchesQuery(columnName, innerQuery);
        return query;
    }
}

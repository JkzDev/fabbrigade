package com.fabbrigade.brigade.shared;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.fabbrigade.brigade.BaseFabBrigadeApplication;
import com.fabbrigade.brigade.BuildConfig;
import com.fabbrigade.brigade.R;
import com.facebook.CallbackManager;
import com.facebook.appevents.AppEventsLogger;
import com.parse.ConfigCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;

import de.greenrobot.event.EventBus;
import timber.log.Timber;

/**
 * Copyright FabBrigade 2015
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected BaseFabBrigadeApplication mApplication;
    protected CallbackManager mCallbackManager;
    protected EventBus mEventBus = EventBus.getDefault();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApplication = (BaseFabBrigadeApplication) getApplication();
        mCallbackManager = CallbackManager.Factory.create();

        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        ParseConfig.getInBackground(new ConfigCallback() {
            @Override
            public void done(ParseConfig config, ParseException e) {
                if (e != null) {
                    Timber.e(e, "Error getting config");
                    return;
                }

                int minVersion = config.getInt("minVersion");
                int curVersion = BuildConfig.VERSION_CODE;
                if (curVersion < minVersion) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(BaseActivity.this)
                                    .setTitle(R.string.title_upgrade_version)
                                    .setMessage(R.string.label_upgrade_version)
                                    .setPositiveButton(R.string.button_play_store, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss(); // TODO: take user to play store
                                        }
                                    })
                                    .setCancelable(false)
                                    .show();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }
}

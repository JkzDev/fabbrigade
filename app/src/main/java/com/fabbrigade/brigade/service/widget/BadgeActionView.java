package com.fabbrigade.brigade.service.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fabbrigade.brigade.R;

/**
 * Copyright FabBrigade 2015
 */
public class BadgeActionView extends RelativeLayout {

    private ImageView mIcon;
    private TextView mBadge;

    public BadgeActionView(Context context) {
        super(context);
        init();
    }

    BadgeActionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    BadgeActionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_actionbar_cart, this);
        setClickable(true);
        mIcon = (ImageView) findViewById(R.id.icon);
        mBadge = (TextView) findViewById(R.id.badge);
    }

    public void setIcon(final Drawable icon) {
        if (mIcon != null) {
            mIcon.setImageDrawable(icon);
        }
    }

    public void setBadge(final int count) {
        if (mBadge != null) {
            if (count == 0) {
                mBadge.setVisibility(GONE);
            } else {
                mBadge.setVisibility(VISIBLE);
                mBadge.setText(count > 99 ? "99+" : String.valueOf(count));
            }
        }
    }
}

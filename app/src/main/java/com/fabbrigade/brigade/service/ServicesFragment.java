package com.fabbrigade.brigade.service;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.category.model.Category;
import com.fabbrigade.brigade.service.model.Service;
import com.fabbrigade.brigade.shared.BaseRecyclerViewFragment;
import com.fabbrigade.brigade.shared.StringUtils;
import com.fabbrigade.brigade.shared.model.Cart;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Copyright FabBrigade 2015
 */
public class ServicesFragment extends BaseRecyclerViewFragment<ServicesFragment.ServicesAdapter> {

    private static final String ARG_CATEGORY_ID = "category_id";

    private ServicesAdapter mAdapter;

    public static ServicesFragment getInstance(String id) {
        final Bundle args = new Bundle();
        args.putString(ARG_CATEGORY_ID, id);
        final ServicesFragment fragment = new ServicesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        ButterKnife.bind(this, view);
        ButterKnife.findById(view, R.id.booking_list_loading).setVisibility(View.GONE);  //TODO remove, put in xml?
        mAdapter = new ServicesAdapter();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Category category = ParseObject.createWithoutData(Category.class, getArguments().getString(ARG_CATEGORY_ID));
        new ParseQuery<>(Service.class)
                .orderByAscending(Service.COLUMN_POSITION)
                .whereEqualTo(Service.COLUMN_CATEGORY, category)
                .findInBackground(new FindCallback<Service>() {
                    @Override
                    public void done(List<Service> services, ParseException e) {
                        if (e != null) {
                            Timber.e(e, "Error fetching categories.");
                            return;
                        }

                        mAdapter.setServices(services);
                    }
                });
    }

    @Override
    protected ServicesAdapter getAdapter() {
        return mAdapter;
    }

    class ServicesAdapter extends RecyclerView.Adapter<ServiceViewHolder> {

        private List<Service> servicesAvailable;

        @Override
        public ServiceViewHolder onCreateViewHolder(ViewGroup view, int position) {
//            final View itemView = LayoutInflater.from(view.getContext()).inflate(R.layout.list_item_services, view, false);

            View itemView = LayoutInflater.from(view.getContext()).inflate(R.layout.list_item_services, null);

//            final ViewGroup.LayoutParams lp = itemView.getLayoutParams();
//            lp.height = (int) Math.floor(ViewUtils.getContentWidth(getActivity()) * 1.25);
//            itemView.setLayoutParams(lp);
            ServiceViewHolder serv =    new ServiceViewHolder(itemView);


            return serv ;
        }

        @Override
        public void onBindViewHolder(final ServiceViewHolder holder, int position) {
            final Service service = servicesAvailable.get(position);
            if(service.getName().toString().equalsIgnoreCase("BRIDAL")){
                if (service.getImageUrl() != null) {
//                    Glide.with(getActivity())
//                            .load(service.getImageUrl())
//                            .centerCrop()
//                            .dontAnimate()
//                            .into(holder.image);

                    Glide.with(getActivity()).load(service.getImageUrl())

                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    holder.progressBar.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .placeholder(R.drawable.placholder)
                            .into(holder.image);


                }
                holder.name.setText(service.getName());
                if (TextUtils.isEmpty(service.getDescription())) {
                    holder.description.setVisibility(View.GONE);
                } else {
                    holder.description.setVisibility(View.VISIBLE);
                    holder.description.setText(service.getDescription());
                }
                holder.duration.setText(StringUtils.getTimeString(getContext(), service.getDuration()));
                holder.price.setText(StringUtils.getCurrencyString(getContext(), service.getPrice()));

                holder.addServiceButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addOrRemoveServiceToCart(service);
                        setButtonText(holder, service);
                    }
                });

                holder.bridal.setVisibility(View.VISIBLE);
                holder.unbridal.setVisibility(View.GONE);


            }else{
                if (service.getImageUrl() != null) {
//                    Glide.with(getActivity())
//                            .load(service.getImageUrl())
//                            .centerCrop()
//                            .dontAnimate()
//                            .into(holder.image);



                    Glide.with(getActivity()).load(service.getImageUrl())

                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    holder.progressBar.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .placeholder(R.drawable.placholder)
                            .into(holder.image);

                }
                holder.name.setText(service.getName());
                if (TextUtils.isEmpty(service.getDescription())) {
                    holder.description.setVisibility(View.GONE);
                } else {
                    holder.description.setVisibility(View.VISIBLE);
                    holder.description.setText(service.getDescription());
                }
                holder.duration.setText(StringUtils.getTimeString(getContext(), service.getDuration()));
                holder.price.setText(StringUtils.getCurrencyString(getContext(), service.getPrice()));

                holder.addServiceButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addOrRemoveServiceToCart(service);
                        setButtonText(holder, service);
                    }
                });



            }



            setButtonText(holder, service);

        }

        private void setButtonText(ServiceViewHolder holder, Service service) {
            Cart cart = ((ServicesActivity) getActivity()).getCart();
            if (cart.containsService(service)) {
                holder.addServiceButton.setText(R.string.button_remove_service);
            } else {
                holder.addServiceButton.setText(R.string.button_book_service);
            }
        }

        @Override
        public int getItemCount() {
            return servicesAvailable == null ? 0 : servicesAvailable.size();
        }

        public void setServices(List<Service> services) {
            servicesAvailable = services;
            notifyDataSetChanged();
        }

        private void addOrRemoveServiceToCart(Service service) {
            Cart cart = ((ServicesActivity)getActivity()).getCart();
            if (cart.containsService(service)) { // TODO animate button for less jarring fade (also make flat)
                service.unpinInBackground();
                cart.removeServiceFromCart(service, getActivity());
            } else {
                service.pinInBackground();
                cart.addServiceToCart(service, getActivity());
            }
        }
    }

    public class ServiceViewHolder extends RecyclerView.ViewHolder {

        //@Bind(R.id.image)
        protected ImageView image;
       // @Bind(R.id.name)
        protected TextView name;
        //@Bind(R.id.description)
        protected TextView description;
        //@Bind(R.id.duration)
        protected TextView duration;
        //@Bind(R.id.price)
        protected TextView price;
        //@Bind(R.id.button_add_service)
        protected Button addServiceButton;
        //@Bind(R.id.bridal)
        protected LinearLayout bridal;
        //@Bind(R.id.unbridal)
        protected RelativeLayout unbridal;

        ProgressBar progressBar;


        public ServiceViewHolder(View view) {
            super(view);
            this.image = (ImageView)view.findViewById(R.id.imgView);
            this.name = (TextView)view.findViewById(R.id.name);
            this.description = (TextView)view.findViewById(R.id.description);
            this.duration = (TextView)view.findViewById(R.id.duration);
            this.price = (TextView)view.findViewById(R.id.price);
            this.addServiceButton = (Button)view.findViewById(R.id.button_add_service);
            this.bridal= (LinearLayout)view.findViewById(R.id.bridal);
            this.unbridal = (RelativeLayout)view.findViewById(R.id.unbridal);

            this.progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
//            ButterKnife.bind(this, view);



        }
    }
}

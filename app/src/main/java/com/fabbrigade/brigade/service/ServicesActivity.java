package com.fabbrigade.brigade.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.booking.BookingActivity;
import com.fabbrigade.brigade.bookingdetails.BookingDetailsActivity;
import com.fabbrigade.brigade.category.model.Category;
import com.fabbrigade.brigade.profile.LoginSplashActivity;
import com.fabbrigade.brigade.service.widget.BadgeActionView;
import com.fabbrigade.brigade.shared.BaseActivity;
import com.fabbrigade.brigade.shared.model.Cart;
import com.fabbrigade.brigade.shared.model.CartWonderfruit;
import com.fabbrigade.brigade.tabpage.wonderfruitVO;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

import static butterknife.ButterKnife.findById;

/**
 * Copyright FabBrigade 2015
 */
public class ServicesActivity extends BaseActivity implements Cart.CartChangedListener,CartWonderfruit.CartChangedListener {

    private static final String ARG_PARENT_ID = "parent_id";
    private static final int RC_FB_LOGIN = 652;
    private static final int RC_BOOKING = 642;

    @Bind(R.id.pager) ViewPager pager;
    @Bind(R.id.tabs) TabLayout tabs;
    @Bind(R.id.loading_bar) ProgressBar progressBar;

    private Cart cart;


    private String categoryId;

    ArrayList<wonderfruitVO>  wonderLst;
    ArrayList<wonderfruitVO>  childLst;


    public static Intent getIntent(Context context, String parentId) {
        final Bundle extras = new Bundle();
        extras.putString(ARG_PARENT_ID, parentId);

        final Intent intent = new Intent(context, ServicesActivity.class);
        intent.putExtras(extras);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        ButterKnife.bind(this);
         final String[] TITLES_WON = { "MAKE-UP", "HAIR", "TOTAL LOOK"};
         final String[] TITLES_GLAM = { "MAKE-UP", "HAIR", "BRIDAL"};

        final Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        categoryId = extras.getString(ARG_PARENT_ID);

        ParseQuery<ParseUser> innerQuery = ParseQuery.getQuery("Category");
        innerQuery.whereMatches("objectId", categoryId);
        displayLoading(true);
        new ParseQuery<>(Category.class)
                .orderByAscending("position")
                .whereMatchesQuery("parent", innerQuery)
                .findInBackground(new FindCallback<Category>() {
                    @Override
                    public void done(List<Category> categories, ParseException e) {
                        displayLoading(false);

                        if (e != null) {
                            Timber.e(e, "Error fetching categories.");
                            return;
                        }

                        if (categories != null && categories.size() > 0) {
                            pager.setAdapter(new ServicesPagerAdapter(getSupportFragmentManager(), categories));
                            tabs.setupWithViewPager(pager);
                        }
                    }
                });




//
//if(Utils.from_menu == 0){
//    new AsyncTask<Void, Void, String>() {
//        JSONArray jsonArray = null;
//        @Override
//        protected String doInBackground(Void... voids) {
//            displayLoading(true);
//            OkHttpClient okHttpClient = new OkHttpClient();
//
//            String data = Utils.OKHttpPostData("http://jakki3zservice.appspot.com/jakki3zservice");
//
//
//            return data;
//        }
//
//
//        @Override
//        protected void onPostExecute(String string) {
//            super.onPostExecute(string);
//            try {
//                displayLoading(false);
//                wonderLst = new ArrayList<>();
//                jsonArray  = new JSONArray(string);
//                for(int i=0;i<jsonArray.length();i++){
//
//
//                    JSONObject datalist = jsonArray.getJSONObject(i);
//                    JSONArray data = datalist.getJSONArray("data");
//                    childLst = new ArrayList<>();
//
//                    for(int x=0;x<data.length();x++){
//                        JSONObject dataObj = data.getJSONObject(x);
//
//                        childLst.add(new wonderfruitVO(
//                                dataObj.getString("Id"),
//                                dataObj.getString("description"),
//                                dataObj.getString("duration"),
//                                dataObj.getString("image"),
//                                dataObj.getString("image_small"),
//                                dataObj.getString("name"),
//                                dataObj.getString("price"),
//                                dataObj.getString("serviceDescription")
//                        ));
//
//                    }
//
//                    wonderLst.add(new wonderfruitVO(
//                            datalist.getString("CateId"),
//                            datalist.getString("name"),
//                            childLst)
//                    );
//
//                }
//                Log.i("size",""+jsonArray.length());
//                pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(),wonderLst,TITLES_WON));
//                tabs.setupWithViewPager(pager);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//        }
//    }.execute();
//
//}else{
//
//
//    new AsyncTask<Void, Void, String>() {
//        JSONArray jsonArray = null;
//        @Override
//        protected String doInBackground(Void... voids) {
//            displayLoading(true);
//            OkHttpClient okHttpClient = new OkHttpClient();
//
//            String data = Utils.OKHttpPostData("http://jakki3zservice.appspot.com/GlamService");
//
//
//            return data;
//        }
//
//
//        @Override
//        protected void onPostExecute(String string) {
//            super.onPostExecute(string);
//            try {
//                displayLoading(false);
//                wonderLst = new ArrayList<>();
//                jsonArray  = new JSONArray(string);
//                for(int i=0;i<jsonArray.length();i++){
//
//
//                    JSONObject datalist = jsonArray.getJSONObject(i);
//                    JSONArray data = datalist.getJSONArray("data");
//                    childLst = new ArrayList<>();
//
//                    for(int x=0;x<data.length();x++){
//                        JSONObject dataObj = data.getJSONObject(x);
//
//                        childLst.add(new wonderfruitVO(
//                                dataObj.getString("Id"),
//                                dataObj.getString("description"),
//                                dataObj.getString("duration"),
//                                dataObj.getString("image"),
//                                dataObj.getString("image_small"),
//                                dataObj.getString("name"),
//                                dataObj.getString("price"),
//                                dataObj.getString("serviceDescription")
//                        ));
//
//                    }
//
//                    wonderLst.add(new wonderfruitVO(
//                            datalist.getString("CateId"),
//                            datalist.getString("name"),
//                            childLst)
//                    );
//
//                }
//                Log.i("size",""+jsonArray.length());
//                pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager(),wonderLst,TITLES_GLAM));
//                tabs.setupWithViewPager(pager);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//
//        }
//    }.execute();
//}
//
//
//





    }

    @Override
    protected void onResume() {
        super.onResume();
        loadCart();
    }

    private void loadCart() {
        cart = new Cart(categoryId, this, this);
    }

    private void displayLoading(Boolean displayLoading) {
        if (displayLoading) {
            tabs.setVisibility(View.GONE);
            pager.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            tabs.setVisibility(View.VISIBLE);
            pager.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_services, menu);
        final BadgeActionView badge = new BadgeActionView(this);
        badge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cart.size() > 0) {
                    launchBookingOrLogin();
                } else {
                    Toast.makeText(ServicesActivity.this, R.string.message_empty_booking, Toast.LENGTH_SHORT).show();
                }
//                launchBookingOrLogin();

            }
        });
        menu.findItem(R.id.action_cart).setActionView(badge);
        return true;
    }
    private void launchBookingOrLogin() {

//        startActivityForResult(BookingActivity.getIntent(ServicesActivity.this, categoryId), RC_BOOKING);

        if (ParseUser.getCurrentUser() == null) {
            startActivityForResult(new Intent(this, LoginSplashActivity.class), RC_FB_LOGIN);
        } else {
            startActivityForResult(BookingActivity.getIntent(ServicesActivity.this, categoryId,ServicesActivity.this), RC_BOOKING);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_FB_LOGIN && resultCode == RESULT_OK) {
            launchBookingOrLogin();
        } else if (requestCode == RC_BOOKING) {
            loadCart();
            if (resultCode == RESULT_OK) {
                Intent i = new Intent(this, BookingDetailsActivity.class);
                i.putExtra(BookingDetailsActivity.ARG_BOOKING_ID,
                        data.getStringExtra(BookingDetailsActivity.ARG_BOOKING_ID));
                startActivity(i);
            }
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final BadgeActionView badge = (BadgeActionView) MenuItemCompat.getActionView(menu.findItem(R.id.action_cart));
        badge.setIcon(ContextCompat.getDrawable(this, R.drawable.shopping_bag));
        badge.setBadge(cart.size());
        Timber.d("Reloaded badge for  " + cart.size() + " services");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    public class MyPagerAdapter extends FragmentPagerAdapter {



          String[] TITLES;

        ArrayList<wonderfruitVO>  wonderLstPager;



        public MyPagerAdapter(FragmentManager fm, ArrayList<wonderfruitVO>  wonderLstPager,String[] TITLES) {
            super(fm);
            this.wonderLstPager = wonderLstPager;
            this.TITLES  = TITLES;


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {

            return null;
        }

    }






    private class ServicesPagerAdapter extends FragmentPagerAdapter {

        private final List<Category> mCategories;

        public ServicesPagerAdapter(FragmentManager fm, List<Category> categories) {
            super(fm);
            mCategories = categories;
        }

        @Override
        public Fragment getItem(int position) {
            final String id = mCategories.get(position).getObjectId();
            return ServicesFragment.getInstance(id);
        }

        @Override
        public int getCount() {
            return mCategories == null ? 0 :mCategories.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mCategories.get(position).getName();
        }
    }

    public Cart getCart() {
        return cart;
    }



    @Override
    public void onCartChanged() {
        invalidateOptionsMenu();
    }


}

package com.fabbrigade.brigade.service.model;

import android.content.Context;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.category.model.Category;
import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.List;

/**
 * Copyright FabBrigade 2015
 */
@ParseClassName("Service")
public class Service extends ParseObject {
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DESCRIPTION = "serviceDescription";
    public static final String COLUMN_DURATION = "duration";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_POSITION = "position";
    public static final String COLUMN_CATEGORY = "category";

    private String mImageUrl;

    public String getName() {
        return getString(COLUMN_NAME);
    }

    public String getDescription() {
        return getString(COLUMN_DESCRIPTION);
    }
    public Category getCategory() {
        return (Category)get(COLUMN_CATEGORY);
    }
    public void setCategory(Category category) {
        if (category != null) {
            put(COLUMN_CATEGORY, category);
        }
    }

    public static String getServiceProvidedString(Context ctx, List<Service> services) {

        String serviceDescription = "";
        int totalServices = services.size();
        int currentService = 1;

        for (Service service : services) {
            String name = service.getName();

            if (currentService == 1) {
                serviceDescription = name;
            } else if (currentService == totalServices-1) {
                serviceDescription = serviceDescription + ctx.getString(R.string.text_rate_service_description_comma) + name;
            } else {
                serviceDescription = serviceDescription + ctx.getString(R.string.text_rate_service_description_and) + name;
            }
            currentService++;
        }

        return serviceDescription;
    }


    public int getDuration() {
        return getInt(COLUMN_DURATION);
    }

    public double getPrice() {
        return getDouble(COLUMN_PRICE);
    }

    public String getImageUrl() {
        return getParseFile(COLUMN_IMAGE) != null ? getParseFile(COLUMN_IMAGE).getUrl(): mImageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Service)) {
            return false;
        }

        return ((Service) o).getObjectId().equals(getObjectId());
    }

}

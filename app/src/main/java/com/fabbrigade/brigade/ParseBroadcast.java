package com.fabbrigade.brigade;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.fabbrigade.brigade.bookingdetails.BookingDetailsActivity;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONObject;

/**
 * Created by Jakki3z on 12/15/15 AD.
 */
public class ParseBroadcast extends ParsePushBroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        ParseAnalytics.trackAppOpenedInBackground(intent);

    }

    @Override
    protected Notification getNotification(Context context, Intent intent) {
        try {
            Bundle bundle = intent.getExtras();
            JSONObject jsObj = new JSONObject(bundle.getString("com.parse.Data"));
//            String type = jsObj.getString("type");
//            String link = jsObj.optString("link","");
//            String id = jsObj.optString("id","");
//            String alert = jsObj.optString("alert","");
//            String detail = jsObj.optString("array_detail","");
//            String open = jsObj.optString("open","");

//            NotificationManager.getInstance().notificationAlert(context,type, link, id, alert,detail,open);
//
//            switch (SharePrefManager.getNotificationSound(context)){
//                case 0:
//                    break;
//                case 1:
//                    break;
//                case 2:
//                    MediaPlayer mp = MediaPlayer.create(context, R.raw.eatigo_pitchshift_harmony_up_no_drums);
//                    mp.start();
//                    break;
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        Notification notification = super.getNotification(context, intent);
        //notification.sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/raw/eatigo_pitchshift_harmony_up_no_drums");
        return notification;
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        try {
            Bundle bundle = intent.getExtras();
            JSONObject jsObj = new JSONObject(bundle.getString("com.parse.Data"));
//            String type = jsObj.getString("type");
//            String link = jsObj.getString("link");
            String id = jsObj.getString("bookingId");
            Intent bookIntent = null;
            bookIntent = new Intent(context, BookingDetailsActivity.class);
//            bookIntent.putExtra("type", type);
            bookIntent.putExtra("booking_id", id);
//            bookIntent.putExtra("url",link);
            bookIntent.setAction(Intent.ACTION_MAIN);
            bookIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            bookIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(bookIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
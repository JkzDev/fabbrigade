package com.fabbrigade.brigade.booking.widget;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.GridLayout;

import com.fabbrigade.brigade.R;

/**
 * Copyright FabBrigade 2015
 */
public class PhoneNumberView extends GridLayout {


    public boolean isValid() {
        return mPhoneNumberView.getEditText() != null && !mPhoneNumberView.getEditText().getText().toString().isEmpty();
    }

    public interface Listener {
        void onTextChanged(String text);
    }

    private TextInputLayout mPhoneNumberView;

    private Listener mListener;

    public PhoneNumberView(Context context) {
        this(context, null);
    }

    public PhoneNumberView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PhoneNumberView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.view_widget_phone_number, this);
        mPhoneNumberView = (TextInputLayout) findViewById(R.id.phone_number);

        final EditText editText = mPhoneNumberView.getEditText();

        if (editText != null) {

            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
                @Override
                public void afterTextChanged(Editable s) {
                    if (mListener != null & s.length() > 3) {
                        mListener.onTextChanged(s.toString());
                    }
                }
            });
        }
    }

    @SuppressWarnings("ConstantConditions")
    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumberView.getEditText().setText(phoneNumber);
    }

    @SuppressWarnings("ConstantConditions")
    public String getPhoneNumber() {
        return mPhoneNumberView.getEditText().getText().toString();
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }
}

package com.fabbrigade.brigade.booking.util;

import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.shared.QueryUtils;
import com.fabbrigade.brigade.shared.model.FabUser;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Date;

import timber.log.Timber;

/**
 * Created by Laptop on 11/17/2015
 */
public class BookingUtils {

    public static void getLastBooking(final onBookingFoundListener onBookingFoundListener) {
        ParseQuery<ParseObject> oldBookingQuery = ParseQuery.getQuery("Booking");
        QueryUtils.addMatchCurrentUserToQuery(oldBookingQuery, false);
        oldBookingQuery.include("brigadette");
        oldBookingQuery.include("creditCard");
        oldBookingQuery.orderByDescending("createdAt");
        oldBookingQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e != null) {
                    switch (e.getCode()) {
                        case 101: // No results found for query
                            Timber.d("No prior bookings found");
                            break;
                        default:
                            Timber.e(e, "Error retrieving booking");
                    }
                } else {
                    onBookingFoundListener.onBookingFound((Booking) object);
                }
            }
        });
    }




    public static void finishBooking(Date bookdate,String address, final Booking booking, final onBookingCompleteListener onBookingCompleteListener) {
        FabUser user = (FabUser) ParseUser.getCurrentUser();
        booking.setClient(user);
        booking.setDate(bookdate);
        booking.setCity("Pattaya");
        booking.setAddress(address);


        booking.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    booking.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e != null) {
                                Timber.e(e, "Failed to save booking in background");
                                onBookingCompleteListener.onBookingComplete(false);
                            } else {
                                Timber.d("Booking saved");
                                onBookingCompleteListener.onBookingComplete(true);
                            }
                        }
                    });

                } else {
                    Timber.e(e, "Booking save failed");
                    onBookingCompleteListener.onBookingComplete(false);

                }

            }
        });
    }

    public interface onBookingCompleteListener {
        void onBookingComplete(Boolean success);
    }

    public interface onBookingFoundListener {
        void onBookingFound(Booking booking);
    }
}

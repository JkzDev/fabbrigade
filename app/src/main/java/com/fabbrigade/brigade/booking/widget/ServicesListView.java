package com.fabbrigade.brigade.booking.widget;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.service.model.Service;
import com.fabbrigade.brigade.shared.StringUtils;

/**
 * Copyright FabBrigade 2015.
 */
public class ServicesListView extends LinearLayout {

    private LinearLayout mListView;
    private TextView mDurationView;
    private TextView mPriceView;

    private int mDuration = 0;
    private double mPrice = 0;

    private Listener mListener;

    public ServicesListView(Context context) {
        this(context, null, 0);
    }

    public ServicesListView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ServicesListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_widget_services_list, this);
        mListView = (LinearLayout) findViewById(R.id.services_list);
        mDurationView = (TextView) findViewById(R.id.total_duration);
        mPriceView = (TextView) findViewById(R.id.total_price);
    }

    public void addService(final Service service) {
        updateTimes(service, true);

        final CardView bookingView = (CardView) LayoutInflater.from(getContext()).inflate(R.layout.widget_booking, this, false);

        ((TextView) bookingView.findViewById(R.id.name)).setText(service.getName());
        ((TextView) bookingView.findViewById(R.id.duration)).setText(StringUtils.getTimeString(getContext(), service.getDuration()));
        ((TextView) bookingView.findViewById(R.id.price)).setText(StringUtils.getCurrencyString(getContext(), service.getPrice()));

        if (service.getImageUrl() != null) {
            Glide.with(getContext())
                    .load(service.getImageUrl())
                    .dontAnimate()
                    .centerCrop()
                    .into((ImageView) bookingView.findViewById(R.id.image));
        }

        mListView.addView(bookingView);
        final int position = mListView.getChildCount() -1;

        bookingView.findViewById(R.id.remove).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                animateOut(position);
                updateTimes(service, false);
                mListener.onRemoveService(service);
            }
        });
    }
    private void updateTimes(Service service, boolean add) {
        if (add) {
            mDuration += service.getDuration();
            mPrice += service.getPrice();
        } else {
            mDuration -= service.getDuration();
            mPrice -= service.getPrice();
        }

        mDurationView.setText(StringUtils.getTimeString(getContext(), mDuration));
        mPriceView.setText(StringUtils.getCurrencyString(getContext(), mPrice));

        Utils.totalDulation = String.valueOf(mDuration);
        Utils.totalPrice = String.valueOf(mPrice);


    }

    private void animateOut(int position) {
        final View view = mListView.getChildAt(position);
        final Animation removeAnimation = AnimationUtils.loadAnimation(view.getContext(), R.anim.slide_out_to_right);
        removeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.startAnimation(removeAnimation);

    }
    public void setListener(Listener listener) {
        mListener = listener;
    }

    public interface Listener {
        void onRemoveService(Service service);
    }
}

package com.fabbrigade.brigade.booking.model;

import android.content.Context;

import com.fabbrigade.brigade.R;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Copyright FabBrigade 2015
 */
@ParseClassName("CreditCard")
public class FabCard extends ParseObject {
    public static final String COLUMN_EXPIRY_MONTH = "expiryMonth";
    public static final String COLUMN_EXPIRY_YEAR = "expiryYear";
    public static final String COLUMN_LAST_DIGITS = "last4Digits";
    public static final String COLUMN_USER = "user";
    public static final String COLUMN_BRAND = "brand";
    public static final String COLUMN_OMISE_CARD_ID = "omiseCardId";

    public void setExpiryMonth(int month) {
        put(COLUMN_EXPIRY_MONTH, month);
    }

    public int getExpiryMonth() {
        return getInt(COLUMN_EXPIRY_MONTH);
    }

    public void setExpiryYear(int year) {
        put(COLUMN_EXPIRY_YEAR, year);
    }

    public int getExpiryYear() {
        return getInt(COLUMN_EXPIRY_YEAR);
    }

    public void setLastDigits(String digits) {
        put(COLUMN_LAST_DIGITS, digits);
    }

    public String getLastDigits() {
        return getString(COLUMN_LAST_DIGITS);
    }

    public String getOmiseCardId() {
        return getString(COLUMN_OMISE_CARD_ID);
    }
    public String getBrand() {
        return getString(COLUMN_BRAND);
    }

    public void setUser(ParseUser user) {
        put(COLUMN_USER, user);
    }

    public String getCardNameDisplay(Context ctx) {
        return ctx.getString(R.string.label_card_number, getBrand(), getLastDigits());
    }
}

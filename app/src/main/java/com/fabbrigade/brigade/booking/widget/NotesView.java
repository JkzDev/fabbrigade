package com.fabbrigade.brigade.booking.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.fabbrigade.brigade.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Copyright FabBrigade 2015
 */
public class NotesView extends LinearLayout {

    private Listener mListener;

    @Bind(R.id.booking_notes_entry) EditText notesText;

    public NotesView(Context context) {
        this(context, null);
    }

    public NotesView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NotesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = inflate(getContext(), R.layout.view_widget_notes, this);
        ButterKnife.bind(view);

        notesText.addTextChangedListener(new SimpleTextWatcher());
    }


    public void setNotes(String notes) {
        notesText.setText(notes);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public String getNotes() {
        return notesText != null ? notesText.getText().toString() : null;
    }

    public interface Listener {
        void onTextChanged(String text);
    }

    public class SimpleTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mListener != null) {
                mListener.onTextChanged(s.toString());
            }
        }
    }
}

package com.fabbrigade.brigade.booking.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.booking.model.FabCard;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Copyright FabBrigade 2015
 */
public class PaymentView extends LinearLayout implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    @Bind(R.id.payment_add_card)
    Button mAddCardView;

    @Bind(R.id.payment_existing_cards)
    Spinner mExistingCardsView;

    private ArrayAdapter<FabCard> mExistingCardsAdapter;
    private Listener mListener;

    public interface Listener {
        void onAddCard();
        void onCardSelected(FabCard card);
    }

    public PaymentView(Context context) {
        this(context, null);
    }

    public PaymentView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PaymentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(getContext(), R.layout.view_widget_payment, this);
        ButterKnife.bind(this);

        mAddCardView.setOnClickListener(this);

        mExistingCardsAdapter = new CardAdapter(getContext());
        mExistingCardsView.setAdapter(mExistingCardsAdapter);
        mExistingCardsView.setOnItemSelectedListener(this);
        mExistingCardsView.setPromptId(R.string.label_change_card);
    }

    public void addCard(FabCard card, Boolean newCard) {
        if (mExistingCardsAdapter.getPosition(card) == -1) {
            mExistingCardsAdapter.add(card);
            mExistingCardsView.setVisibility(VISIBLE);
        }
        if (newCard) {
            int position = mExistingCardsAdapter.getPosition(card);
            mExistingCardsView.setSelection(position);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (mListener != null) {
            mListener.onCardSelected(mExistingCardsAdapter.getItem(position));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.payment_add_card:
                if (mListener != null) {
                    mListener.onAddCard();
                }
                break;
        }
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    class CardAdapter extends ArrayAdapter<FabCard> {

        public CardAdapter(Context context) {
            super(context, android.R.layout.simple_dropdown_item_1line);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null) {
                view = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
            }

            final FabCard card = getItem(position);
            final TextView text = (TextView) view.findViewById(android.R.id.text1);
            text.setText(card.getCardNameDisplay(getContext()));

            return view;
        }

        @Override
        public View getDropDownView(int position, View view, ViewGroup parent) {
            if (view == null) {
                view = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_single_choice, parent, false);
            }

            final FabCard card = getItem(position);
            final TextView text = (TextView) view.findViewById(android.R.id.text1);
            text.setText(card.getCardNameDisplay(getContext()));

            return view;
        }
    }
}

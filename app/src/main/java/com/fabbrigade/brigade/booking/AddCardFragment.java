package com.fabbrigade.brigade.booking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.booking.model.FabCard;
import com.fabbrigade.brigade.booking.util.CardUtils;
import com.fabbrigade.brigade.shared.BaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.omise.Card;

/**
 * Copyright FabBrigade 2015
 */
public class AddCardFragment extends BaseFragment {

    @Bind(R.id.button_add_card) Button addButton;
    @Bind(R.id.add_card_name) TextInputLayout cardName;
    @Bind(R.id.add_card_number) TextInputLayout cardNumber;
    @Bind(R.id.add_card_expiration_month) TextInputLayout cardMonth;
    @Bind(R.id.add_card_expiration_year) TextInputLayout cardYear;
    @Bind(R.id.add_card_cvv) TextInputLayout cardSecurityCode;
    @Bind(R.id.add_card_postal_code) TextInputLayout cardPostalCode;
    @Bind(R.id.add_card_city) TextInputLayout cardCity;
    @Bind(R.id.card_submission) ProgressBar cardSubmissionSpinner;

    private Card card;
    private boolean validCard = false;

    @SuppressWarnings("ConstantConditions")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_add_card, container, false);
        ButterKnife.bind(this, view);

        cardNumber.getEditText().addTextChangedListener(new CreditCardFormatter());
        cardMonth.getEditText().addTextChangedListener(new GeneralFormatter(cardMonth, 1));
        cardYear.getEditText().addTextChangedListener(new GeneralFormatter(cardYear, 2));
        cardSecurityCode.getEditText().addTextChangedListener(new GeneralFormatter(cardSecurityCode, 3));
        cardPostalCode.getEditText().addTextChangedListener(new GeneralFormatter(cardPostalCode, 5));
        cardCity.getEditText().addTextChangedListener(new GeneralFormatter(cardCity, 1));

        updateButton();

        //Utilized for speed testing
//        autoInsert();

        return view;
    }
    private void autoInsert() {

        card = new Card();
        card.setName("JOHN DOE22");
        card.setCity("Bangkok");
        card.setPostalCode("10320");
        card.setNumber("4242424242424242");
        card.setExpirationMonth("11");
        card.setExpirationYear("2017");
        card.setSecurityCode("123");
        //Need to update w/new code
    }

    @SuppressWarnings("ConstantConditions")
    public void updateButton() {
//        Boolean o = !cardNumber.isErrorEnabled();
//        Boolean o1 = !cardMonth.isErrorEnabled();
//        Boolean o2 = !cardYear.isErrorEnabled();
//        Boolean o3 = !cardSecurityCode.isErrorEnabled();
//        Boolean o4 = !cardPostalCode.isErrorEnabled();
//        Boolean o5 = !cardName.isErrorEnabled();
//        Boolean o6 = cardNumber.isErrorEnabled();
//        Boolean o7 = cardNumber.getEditText().getText().length()>0;
//        Boolean o8 = cardMonth.getEditText().getText().length()>0;
//        Boolean o9 = cardYear.getEditText().getText().length()>0;
//        Boolean o10 = cardSecurityCode.getEditText().getText().length()>0;
//        Boolean o11 = cardPostalCode.getEditText().getText().length()>0;
//        Boolean o12 = cardName.getEditText().getText().length()>0;

        if (!cardNumber.isErrorEnabled() && !cardMonth.isErrorEnabled() && !cardYear.isErrorEnabled() &&
                !cardSecurityCode.isErrorEnabled() && !cardPostalCode.isErrorEnabled() && !cardName.isErrorEnabled()
                && cardNumber.getEditText().getText().length()>0 && cardMonth.getEditText().getText().length()>0 &&
                cardYear.getEditText().getText().length()>0
                && cardSecurityCode.getEditText().getText().length()>0 && cardPostalCode.getEditText().getText().length()>0
                && cardName.getEditText().getText().length()>0) {
            validCard = true;
        } else {
            validCard = false;
        }

    }
    @SuppressWarnings({"unused", "ConstantConditions"})
    @OnClick(R.id.button_add_card)
    public void onAddCard() {

        if (!validCard) {
            Snackbar.make(getView(), getString(R.string.card_invalid), Snackbar.LENGTH_SHORT).show();
            return;
        }

        card = new Card();
        card.setName(cardName.getEditText().getText().toString().trim());

        Editable number = cardNumber.getEditText().getText();

        String spaceChar = String.valueOf(CreditCardFormatter.space);
        String numberNoSpacing = number.toString().replace(spaceChar, "");
        card.setNumber(numberNoSpacing);

        String month = cardMonth.getEditText().getText().toString();
        if (month.length() == 1) {
            month = "0" + month;
        }
        card.setExpirationMonth(month);
        card.setExpirationYear("20" + cardYear.getEditText().getText().toString());
        card.setSecurityCode(cardSecurityCode.getEditText().getText().toString());
        card.setPostalCode(cardPostalCode.getEditText().getText().toString());
        card.setCity(cardCity.getEditText().getText().toString().trim());

        cardSubmissionSpinner.setVisibility(View.VISIBLE);
        CardUtils.addCreditCardForUser(card, new CardUtils.CardAddedListener() {
            @Override
            public void onCardAdded(Boolean resultSuccess, String errorCode, FabCard fabCard) {
                cardSubmissionSpinner.setVisibility(View.GONE);
                if (resultSuccess) {
                    finishActivity(fabCard.getObjectId());
                } else {
                    Toast.makeText(getActivity(), resultSuccess + errorCode, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private void finishActivity(String cardId) {
        Intent intent = new Intent();
        intent.putExtra(AddCardActivity.ARG_CARD_ID, cardId);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    public class GeneralFormatter implements TextWatcher {

        TextInputLayout editText;
        int count;

        public GeneralFormatter(TextInputLayout editText, int count) {
            this.editText = editText;
            this.count = count;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            Boolean selectedTooHigh = false;
            if (editText == cardMonth && s.length() > 0) {
                int value = Integer.valueOf(s.toString());
                if (value == 0 || value > 12) {
                    selectedTooHigh = true;
                }
            }


            if (s.length() < count || selectedTooHigh) {
                editText.setError(getString(R.string.error_card));
            } else {
                editText.setErrorEnabled(false);
            }
            updateButton();
        }
    }

    public class CreditCardFormatter implements TextWatcher {

        public static final char space = ' ';

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char

            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }

            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
            if (s.length() < 19) {
                cardNumber.setError(getString(R.string.error_card));
            } else {
                cardNumber.setErrorEnabled(false);
            }
            updateButton();
        }
    }

}

package com.fabbrigade.brigade.booking;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.shared.BaseRecyclerViewFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import icepick.State;
import timber.log.Timber;

import static butterknife.ButterKnife.findById;

/**
 * Copyright FabBrigade 2015
 */
public class AddressSearchFragment extends BaseRecyclerViewFragment<AddressSearchFragment.AddressSearchAdapter> implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_PLAY_SERVICES = 1001;
    private static final int REQUEST_RESOLVE_ERROR = 1002;

    private static final String DIALOG_ERROR = "dialog_error";

    private GoogleApiClient mGoogleApiClient;

    private SearchView mSearchView;

    private AddressSearchFragment.AddressSearchAdapter mAdapter;

    @State
    boolean mResolvingError;

    private LatLngBounds mLatLngBounds;

    private AutocompleteFilter mFilter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final int playServicesStatus = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());
        if (playServicesStatus != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(playServicesStatus, getActivity(), REQUEST_PLAY_SERVICES);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PLAY_SERVICES && resultCode != Activity.RESULT_OK) {
            final int playServicesStatus = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getContext());
            if (playServicesStatus != ConnectionResult.SUCCESS) {
                GooglePlayServicesUtil.getErrorDialog(playServicesStatus, getActivity(), REQUEST_PLAY_SERVICES);
            }
        } else if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == Activity.RESULT_OK) {
                if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Places.GEO_DATA_API)
                .build();
        mLatLngBounds = new LatLngBounds(new LatLng(5.5, 97.5), new LatLng(20.5, 105.9));
        mAdapter = new AddressSearchFragment.AddressSearchAdapter(mGoogleApiClient);
        List<Integer> filterList = new ArrayList<>();
//        filterList.add(Place.TYPE_STREET_ADDRESS); Not a filterable place
        mFilter = AutocompleteFilter.create(filterList);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_address_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSearchView = findById(view, R.id.search);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    new AsyncTask<String, Void, AutocompletePredictionBuffer>() {

                        @Override
                        protected AutocompletePredictionBuffer doInBackground(String... params) {

                            PendingResult<AutocompletePredictionBuffer> result = Places.GeoDataApi
                                    .getAutocompletePredictions(mGoogleApiClient, params[0], mLatLngBounds, mFilter);

                            return result.await();
                        }

                        @Override
                        protected void onPostExecute(AutocompletePredictionBuffer predictions) {
                            if (predictions.getStatus().isSuccess()) {
                                mAdapter.setResults(DataBufferUtils.freezeAndClose(predictions));
                            } else {
                                Timber.e("Address prediction failed: " + predictions.getStatus().getStatusMessage());
                            }
                        }
                    }.execute(query);
                }

                return true;
            }
        });
        mSearchView.requestFocus();
    }
    //TODO release AutocompletePredictionBuffer
    @Override
    protected AddressSearchFragment.AddressSearchAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void onConnected(Bundle bundle) {
        mSearchView.setEnabled(true);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mSearchView.setEnabled(false);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError) {
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(getActivity(), REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                mGoogleApiClient.connect();
            }
        } else {
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }
    }

    class AddressSearchAdapter extends RecyclerView.Adapter<AddressSearchViewHolder> {

        private GoogleApiClient mClient;
        private ArrayList<AutocompletePrediction> mResults;

        public AddressSearchAdapter(GoogleApiClient client) {
            mClient = client;
        }

        @Override
        public AddressSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new AddressSearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_activated_1, parent, false));
        }

        @Override
        public void onBindViewHolder(AddressSearchViewHolder holder, int position) {
            final AutocompletePrediction prediction = mResults.get(position);
            holder.address.setText(prediction.getFullText(null));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Places.GeoDataApi.getPlaceById(mClient, prediction.getPlaceId())
                            .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                @Override
                                public void onResult(PlaceBuffer places) {
                                    if (places.getStatus().isSuccess()) {
                                        final Place place = places.get(0);

                                        Bundle result = new Bundle();
                                        String address = place.getAddress().toString();
                                        String[] addressSplit = address.split(", ");
                                        result.putString(BookingFragment.ARG_ADDRESS_1, addressSplit[0]);
                                        if (addressSplit.length > 1) {
                                            result.putString(BookingFragment.ARG_CITY, addressSplit[1]);
                                        }
                                        if (addressSplit.length > 2) {
                                            result.putString(BookingFragment.ARG_COUNTRY, addressSplit[2]);
                                        }


                                        LatLng test = place.getLatLng();
                                        Geocoder code = new Geocoder(getActivity(), Locale.getDefault());
                                        try {
                                            List<Address> addresses = code.getFromLocation(test.latitude, test.longitude, 1);
                                            Address singleAddress = addresses.get(0);
                                            result.putParcelable(BookingFragment.ARG_ADDRESS, singleAddress);
                                        } catch (IOException e) {
                                            Timber.e(e, "Failed to get Geocode address");
                                        }
                                        places.release();

                                        Intent intent = new Intent();
                                        intent.putExtras(result);
                                        getActivity().setResult(Activity.RESULT_OK, intent);
                                        getActivity().finish();
                                    }
                                }
                            });
                }
            });
        }

        @Override
        public int getItemCount() {
            return mResults == null ? 0 : mResults.size();
        }

        public void setResults(ArrayList<AutocompletePrediction> results) {
            mResults = results;
            notifyDataSetChanged();
        }
    }

    static class AddressSearchViewHolder extends RecyclerView.ViewHolder {

        TextView address;

        public AddressSearchViewHolder(View itemView) {
            super(itemView);
            address = findById(itemView, android.R.id.text1);
        }
    }

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        final DialogFragment dialogFragment = new DialogFragment() {
            @NonNull
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                int errorCode = this.getArguments().getInt(DIALOG_ERROR);
                return GoogleApiAvailability.getInstance().getErrorDialog(
                        this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
            }

            @Override
            public void onDismiss(DialogInterface dialog) {
                super.onDismiss(dialog);
                onDialogDismissed();
            }
        };

        final Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }
}

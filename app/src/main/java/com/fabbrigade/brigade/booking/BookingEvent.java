package com.fabbrigade.brigade.booking;

/**
 * Created by Laptop on 11/17/2015.
 */
public class BookingEvent {
    private boolean success;
    private String additional;

    public BookingEvent(boolean success, String additional) {
        this.success = success;
        this.additional = additional;
    }
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getAdditional() {
        if (additional == null) {
            return "";
        }
        return additional;
    }

    public void setAdditional(String additional) {
        this.additional = additional;
    }
}

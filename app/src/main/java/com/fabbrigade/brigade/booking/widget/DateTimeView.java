package com.fabbrigade.brigade.booking.widget;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TimePicker;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Copyright FabBrigade 2015
 */
public class DateTimeView extends GridLayout implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    @Bind(R.id.appointment_date)
    EditText mDateView;

    @Bind(R.id.appointment_time)
    EditText mTimeView;
    TimeZone timeZone = TimeZone.getTimeZone("GMT");
    private Calendar mCalendar = Calendar.getInstance(timeZone);
    private Listener mListener;

    private java.text.DateFormat mDateFormat;
    private String mTimeFormat = "HH:mm";

    public interface Listener {
        void onDateTimeChanged();
    }

    public DateTimeView(Context context) {
        this(context, null);
    }

    public DateTimeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DateTimeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_widget_date_time, this);
        ButterKnife.bind(this);

        mDateFormat = DateFormat.getDateFormat(getContext());

        mCalendar.set(Calendar.MILLISECOND, 0);

        mDateView.setFocusable(false);
        mDateView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datepicker =     new DatePickerDialog(getContext(), DateTimeView.this, year, month, day);

                calendar.add(Calendar.DATE, 4);

                datepicker.getDatePicker().setMaxDate(calendar.getTimeInMillis());

                // Subtract 6 days from Calendar updated date
                calendar.add(Calendar.DATE, -4);

                // Set the Calendar new date as minimum date of date picker
                datepicker.getDatePicker().setMinDate(calendar.getTimeInMillis());


                datepicker.show();


            }
        });

        mTimeView.setFocusable(false);
        mTimeView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                ;


                new TimePickerDialog(getContext(), DateTimeView.this, calendar.getTime().getHours(), calendar.getTime().getMinutes(), true)
                        .show();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mCalendar.set(Calendar.YEAR, year);
        mCalendar.set(Calendar.MONTH, monthOfYear);
        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);



        android.text.format.DateFormat.format("yyyy-MM-dd", new java.util.Date());

        mDateView.setText(year+"-"+monthOfYear+"-"+dayOfMonth);
        Utils.date = mDateView.getText().toString();






        if (mListener != null) {
            mListener.onDateTimeChanged();
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mCalendar.set(Calendar.MINUTE, minute);



        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(mTimeFormat);
        if(String.valueOf(minute).length()==1){

            mTimeView.setText(hourOfDay+":0"+minute);
        }else{
            mTimeView.setText(hourOfDay+":"+minute);
        }



        Utils.time = mTimeView.getText().toString();


        if (mListener != null) {
            mListener.onDateTimeChanged();
        }
    }

    public boolean isValid() {
        return mCalendar.after(Calendar.getInstance());
    }

    public Date getDate() {
        return mCalendar.getTime();
    }


    public void setListener(Listener listener) {
        mListener = listener;
    }
}

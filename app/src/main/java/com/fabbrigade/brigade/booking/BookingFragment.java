package com.fabbrigade.brigade.booking;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.booking.model.FabCard;
import com.fabbrigade.brigade.booking.util.BookingUtils;
import com.fabbrigade.brigade.booking.util.CardUtils;
import com.fabbrigade.brigade.booking.widget.AddressView;
import com.fabbrigade.brigade.booking.widget.DateTimeView;
import com.fabbrigade.brigade.booking.widget.NotesView;
import com.fabbrigade.brigade.booking.widget.PaymentView;
import com.fabbrigade.brigade.booking.widget.PhoneNumberView;
import com.fabbrigade.brigade.booking.widget.ServicesListView;
import com.fabbrigade.brigade.me.MyHistoryDetail;
import com.fabbrigade.brigade.service.model.Service;
import com.fabbrigade.brigade.shared.BaseFragment;
import com.fabbrigade.brigade.shared.model.Cart;
import com.fabbrigade.brigade.shared.model.FabUser;
import com.fabbrigade.brigade.tabpage.WonderfruitMapActivity;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.omise.Card;
import icepick.State;
import timber.log.Timber;

/**
 * Copyright FabBrigade 2015.
 */
public class BookingFragment extends BaseFragment implements View.OnClickListener, Cart.CartChangedListener {

    private static final int STATE_ADDRESS = 1;
    private static final int STATE_PHONE_NUMBER = 2;
    private static final int STATE_APPOINTMENT_TIME = 3;
    private static final int STATE_PAYMENT_INFO = 4;
    private static final int STATE_CARD_SELECTED = 5;
    private static final int STATE_DONE = 6;

    private static final int REQUEST_CODE_ADDRESS = 1;
    private static final int REQUEST_CODE_ADD_CARD = 2;

    public static final String ARG_ADDRESS_1 = "address_1";
    public static final String ARG_CITY = "city";
    public static final String ARG_COUNTRY = "country";
    public static final String ARG_ADDRESS = "address";

    private static final double MAX_PROGRESS = 5.0;


    ProgressDialog  pd;
    @Bind(R.id.open_map)
    ImageView openMap;
    @Bind(R.id.get_map_name)
    EditText getMapName;

    private boolean BOOKING_COMPLETE = false;

    private Cart cart;

    String cardIdTest;

    @Bind(R.id.booking_progress)
    ProgressBar mProgress;
    @Bind(R.id.booking_services)
    ServicesListView mServicesListView;
    @Bind(R.id.booking_address)
    AddressView mAddressView;
    @Bind(R.id.booking_phone)
    PhoneNumberView mPhoneNumberView;
    @Bind(R.id.booking_date_time)
    DateTimeView mDateTimeView;
    @Bind(R.id.booking_payment)
    PaymentView mPaymentView;
    @Bind(R.id.button_request_booking)
    Button mRequestBookingButton;
    @Bind(R.id.booking_submission)
    ProgressBar bookingSubmissionSpinner;
    @Bind(R.id.booking_terms)
    LinearLayout mTermsView;
    @Bind(R.id.booking_notes)
    NotesView mNotesView;

    Booking mBooking;

    private boolean mCardPrefilled = false;

    @State
    int mState;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (savedInstanceState == null) {
            mBooking = (Booking) ParseObject.create("Booking");
            mBooking.setClient((FabUser) ParseUser.getCurrentUser());

            Bundle bundle = getActivity().getIntent().getExtras();
            if (bundle != null) {
                String categoryId = bundle.getString(BookingActivity.ARG_CATEGORY_ID);
                cart = new Cart(categoryId, getActivity(), this);

            }
        }

        pd = new ProgressDialog(getActivity());
        pd.setTitle("Processing...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);
        pd.setIndeterminate(true);





//        checkForOldBookingDetails();
    }

    private void checkForOldBookingDetails() {
        BookingUtils.getLastBooking(new BookingUtils.onBookingFoundListener() {
            @Override
            public void onBookingFound(Booking oldBooking) {
                Timber.d("Booking details loaded");
                mAddressView.setAddress1(oldBooking.getAddress());
                mAddressView.setApartmentNumber(oldBooking.getApartment());
                mAddressView.setCity(oldBooking.getCity());
                mAddressView.setPostalCode(oldBooking.getPostalCode());
                mPhoneNumberView.setPhoneNumber(oldBooking.getPhoneNumber());
                FabCard card = oldBooking.getCreditCard();
                if (card != null) {
                    mPaymentView.addCard(oldBooking.getCreditCard(), false);
                }

            }
        });

        CardUtils.fetchAllCards(new CardUtils.CardFetchListener() {
            @Override
            public void onCardsFetched(List<ParseObject> objects) {
                for (ParseObject object : objects) {
                    mPaymentView.addCard((FabCard) object, false);
                }
            }
        });

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_booking, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Card card = new Card();
        card.setName("JOHN DOE22");
        card.setCity("Bangkok");
        card.setPostalCode("10320");
        card.setNumber("4242424242424242");
        card.setExpirationMonth("11");
        card.setExpirationYear("2017");
        card.setSecurityCode("123");

        CardUtils.addCreditCardForUser(card, new CardUtils.CardAddedListener() {
            @Override
            public void onCardAdded(Boolean resultSuccess, String errorCode, FabCard fabCard) {


                if (resultSuccess) {
                    if (mState < STATE_CARD_SELECTED) {
                    mState = STATE_CARD_SELECTED;
                        cardIdTest = fabCard.getObjectId();
//                        mRequestBookingButton.setOnClickListener(BookingFragment.this);
                }

                } else {
//                    Toast.makeText(getActivity(), resultSuccess + errorCode, Toast.LENGTH_LONG).show();
                    cardIdTest="";
                }
            }
        });







        openMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),WonderfruitMapActivity.class);
                startActivity(intent);
            }
        });

//        mAddressView.setListener(new AddressView.Listener() {
//            @Override
//            public void onClick(View v) {
//                startActivityForResult(new Intent(getActivity(), AddressSearchActivity.class), REQUEST_CODE_ADDRESS);
//            }
//
//            @Override
//            public void onTextChanged(String text) {
//                if (mAddressView.isValid()) {
//                    if (mState < STATE_PHONE_NUMBER) {
//                        mState = STATE_PHONE_NUMBER;
//                    }
//
//                    mBooking.setAddress(mAddressView.getAddress1());
//                    mBooking.setPostalCode(mAddressView.getPostalCode());
//                    mBooking.setCity(mAddressView.getCity());
//                    mBooking.setApartment(mAddressView.getApartment());
//                    updateViews();
//                }
//            }
//        });


        mPhoneNumberView.setListener(new PhoneNumberView.Listener() {
            @Override
            public void onTextChanged(String text) {
                if (mPhoneNumberView.isValid()) {
//                    if (mState < STATE_APPOINTMENT_TIME) {
//                        mState = STATE_APPOINTMENT_TIME;
//                    }

                    mBooking.setPhoneNumber(text);
                    updateViews();
                }
            }
        });

        mDateTimeView.setListener(new DateTimeView.Listener() {
            @Override
            public void onDateTimeChanged() {
                if (mDateTimeView.isValid()) {
//                    if (mState < STATE_PAYMENT_INFO) {
//                        mState = STATE_PAYMENT_INFO;
//                    }
//                    if (mCardPrefilled) {
//                        mState = STATE_CARD_SELECTED;
//                    }

                    mBooking.setDate(mDateTimeView.getDate());
                    updateViews();
                }
            }
        });

//        mPaymentView.setListener(new PaymentView.Listener() {
//            @Override
//            public void onAddCard() {
//                Intent intent = new Intent(getContext(), AddCardActivity.class);
//                startActivityForResult(intent, REQUEST_CODE_ADD_CARD);
//            }
//
//            @Override
//            public void onCardSelected(FabCard fabCard) {
//                mBooking.setCreditCard(fabCard);
//
//                if (mState < STATE_CARD_SELECTED) {
//                    mState = STATE_CARD_SELECTED;
//                }
//                updateViews();
//            }
//        });

        mServicesListView.setListener(new ServicesListView.Listener() {
            @Override
            public void onRemoveService(Service service) {
                cart.removeServiceFromCart(service, getActivity());
            }
        });

        mNotesView.setListener(new NotesView.Listener() {

            @Override
            public void onTextChanged(String text) {
                mBooking.setNote(text);
            }
        });
    }


    private void displayServices() {
        if (mBooking != null) {
            for (int i = 0; i < mBooking.getServices().size(); i++) {
                Service service = mBooking.getServices().get(i);
                mServicesListView.addService(service);
            }




            if (mBooking.getNote() != null) {
                mState = STATE_DONE;
            } else if (mBooking.getCreditCard() != null) {
                mState = STATE_CARD_SELECTED;
            } else if (mBooking.getDate() != null) {
                mState = STATE_PAYMENT_INFO;
            } else if (mBooking.getPhoneNumber() != null) {
                mState = STATE_ADDRESS;
            } else {
                mState = STATE_ADDRESS;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);




        if (requestCode == REQUEST_CODE_ADDRESS && resultCode == Activity.RESULT_OK) {
            mAddressView.setAddress1(data.getStringExtra(ARG_ADDRESS_1));
            mAddressView.setCity(data.getStringExtra(ARG_CITY));

            try {
                Address address = data.getParcelableExtra(ARG_ADDRESS);
                mAddressView.setPostalCode(address.getPostalCode());
            } catch (Exception e) {
                Timber.e(e, "Failed to parse Address");
            }

            updateViews();
        } else if (requestCode == REQUEST_CODE_ADD_CARD && resultCode == Activity.RESULT_OK) {

            mPhoneNumberView.requestFocus();

            String cardId = data.getStringExtra(AddCardActivity.ARG_CARD_ID);

//            CardUtils.getCard(cardId, new CardUtils.CardFoundListener() {
//                @Override
//                public void onCardFound(FabCard card) {
//                    mPaymentView.addCard(card, true);
//                }
//            });

            if (mState < STATE_CARD_SELECTED) {
                mState = STATE_CARD_SELECTED;
            }

            updateViews();
        }
    }

    private void updateViews() {
        if (mBooking == null) {
            return;
        }

        switch (mState) {
            case STATE_ADDRESS:
                mAddressView.setVisibility(View.VISIBLE);
                break;
            case STATE_PHONE_NUMBER:
                mAddressView.setVisibility(View.VISIBLE);
                mPhoneNumberView.setVisibility(View.VISIBLE);
                mPhoneNumberView.setPhoneNumber(getString(R.string.phone_prefix));
                break;
            case STATE_APPOINTMENT_TIME:
                mAddressView.setVisibility(View.VISIBLE);
                mPhoneNumberView.setVisibility(View.VISIBLE);
                mDateTimeView.setVisibility(View.VISIBLE);
                break;
            case STATE_PAYMENT_INFO:
                mAddressView.setVisibility(View.VISIBLE);
                mPhoneNumberView.setVisibility(View.VISIBLE);
                mDateTimeView.setVisibility(View.VISIBLE);
//                mPaymentView.setVisibility(View.VISIBLE);
                break;
            case STATE_CARD_SELECTED:
                mAddressView.setVisibility(View.VISIBLE);
                mPhoneNumberView.setVisibility(View.VISIBLE);
                mDateTimeView.setVisibility(View.VISIBLE);
//                mPaymentView.setVisibility(View.VISIBLE);
                mTermsView.setVisibility(View.VISIBLE);
                mNotesView.setVisibility(View.VISIBLE);
                mRequestBookingButton.setVisibility(View.VISIBLE);
                mRequestBookingButton.setOnClickListener(this);
                break;
        }

        mProgress.setProgress((int) Math.round((mState / MAX_PROGRESS) * 100));
    }

    private void submitBooking() {
        BookingUtils.finishBooking(mDateTimeView.getDate(),getMapName.getText().toString(),mBooking, new BookingUtils.onBookingCompleteListener() {
            @Override
            public void onBookingComplete(Boolean success) {
                bookingSubmissionSpinner.setVisibility(View.GONE);
                String text;

                if (success) {
                    text = getString(R.string.booking_complete);
                    BOOKING_COMPLETE = true;
                    cart.emptyCart(getContext());
                    ((BookingActivity) getActivity()).saveBookingIdAndFinishActivity(mBooking.getObjectId());
                } else {
                    text = getString(R.string.error_booking_save);
                }
                Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
       // if (v == mRequestBookingButton) {
            bookingSubmissionSpinner.setVisibility(View.VISIBLE);

        FabUser user = (FabUser) ParseUser.getCurrentUser();
        String address = getMapName.getText().toString();
        String totalPrice = Utils.totalPrice;
        String date = Utils.date.replace("/","-")+" "+Utils.time;
        String totalDu = Utils.totalDulation;
        String bookdetail = "";
        String userName = user.getFullName(getActivity());
        String eamil = user.getEmail();
        String phone = mBooking.getPhoneNumber();




        for (int i = 0; i < mBooking.getServices().size() ; i++) {
            Service service = mBooking.getServices().get(i);


            mServicesListView.addService(service);

            if (i != mBooking.getServices().size() - 1) {
                bookdetail = bookdetail + service.getName() + "," + service.getDuration() + "," + service.getPrice() + "|";
            } else {

                bookdetail = bookdetail + service.getName() + "," + service.getDuration() + "," + service.getPrice() ;
            }

        }


//        submitBooking();

        addBooking(address,totalPrice,date,totalDu,bookdetail,userName,eamil,phone);


//

        //}
    }

    @Override
    public void onCartChanged() {
        if (!BOOKING_COMPLETE) {
            Timber.d("Cart Changed");
            mBooking.setServices(cart.getServices());
            displayServices();
            updateViews();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!Utils.map_name.equalsIgnoreCase("")){

            getMapName.setText(Utils.map_name);
        }


    }

    public void addBooking(final String address, final String price, final String booking_dt,final String bookdulation, final String booking_detail ,
                           final String user_name, final String user_email, final String user_phone) {
//        pd.show();

        new AsyncTask<Void, Void, String>() {


            @Override
        protected String doInBackground(Void... voids) {
            OkHttpClient okHttpClient = new OkHttpClient();

            String data = Utils.PostBookData(address,price,booking_dt,bookdulation,booking_detail,user_name,user_email,user_phone);


            return data;
        }


        @Override
        protected void onPostExecute(String string) {
            super.onPostExecute(string);
            try {
//                pd.dismiss();
                JSONObject data = new JSONObject(string);
                String text ="";
                if(data.getString("status").equalsIgnoreCase("200")){

                    text = data.getString("item");
                    BOOKING_COMPLETE = true;
                    cart.emptyCart(getContext());

                    Intent intent = new Intent(getActivity(), MyHistoryDetail.class);
                    intent.putExtra("Id", data.getString("id"));

                    startActivity(intent);
                    getActivity().finish();



                }else{
                    text = getString(R.string.error_booking_save);
                }


                Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();



            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }.execute();
    }



}

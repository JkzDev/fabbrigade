package com.fabbrigade.brigade.booking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.bookingdetails.BookingDetailsActivity;
import com.fabbrigade.brigade.service.ServicesActivity;
import com.fabbrigade.brigade.shared.BaseActivity;

public class BookingActivity extends BaseActivity {

    public static final String ARG_CATEGORY_ID = "cart";
    public static ServicesActivity servicesAct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_toolbar);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_activity_booking);
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new BookingFragment())
                    .commit();
        }
    }
    protected void saveBookingIdAndFinishActivity(String bookingId) {
        Intent i = new Intent();
        i.putExtra(BookingDetailsActivity.ARG_BOOKING_ID, bookingId);
        setResult(Activity.RESULT_OK, i);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static Intent getIntent(Context context, String categoryId, ServicesActivity servicesActivity) {
        final Bundle extras = new Bundle();
        BookingActivity.servicesAct = servicesActivity;
        extras.putString(ARG_CATEGORY_ID, categoryId);
        final Intent intent = new Intent(context, BookingActivity.class);
        intent.putExtras(extras);
        return intent;
    }


}

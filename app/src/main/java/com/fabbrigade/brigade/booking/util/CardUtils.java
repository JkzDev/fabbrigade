package com.fabbrigade.brigade.booking.util;

import com.fabbrigade.brigade.BuildConfig;
import com.fabbrigade.brigade.booking.model.FabCard;
import com.fabbrigade.brigade.shared.QueryUtils;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.omise.Card;
import co.omise.Omise;
import co.omise.OmiseException;
import co.omise.RequestTokenCallback;
import co.omise.Token;
import co.omise.TokenRequest;
import timber.log.Timber;

/**
 * Created by Laptop on 11/11/2015
 */
public class CardUtils {

    public static void addCreditCardForUser(Card omiseCardSelected, final CardAddedListener cardAddedListener) {
        TokenRequest tokenRequest = new TokenRequest();
        tokenRequest.setPublicKey(BuildConfig.OMISE_PUBLIC_KEY);
        tokenRequest.setCard(omiseCardSelected);
        final Omise omise = new Omise();

        try {
            omise.requestToken(tokenRequest, new RequestTokenCallback() {
                @Override
                public void onRequestSucceeded(Token token) {
                    Timber.d("User token created");
                    Map<String, String> extras = new HashMap<>();
                    extras.put("user", ParseUser.getCurrentUser().getObjectId());
                    extras.put("token", token.getId());

                    ParseCloud.callFunctionInBackground("addCreditCardForUser", extras, new FunctionCallback<FabCard>() {
                        @Override
                        public void done(FabCard card, ParseException e) {
                            if (e == null) {
                                Timber.d("Card Added " + card.getObjectId() + card.getExpiryMonth());
                                cardAddedListener.onCardAdded(true, null, card);
                            } else {
                                cardAddedListener.onCardAdded(false, "- omise creation fail", null);
                                Timber.e(e, "Card add failed");
                            }
                        }
                    });
                }

                @Override
                public void onRequestFailed(final int errorCode) {
                    cardAddedListener.onCardAdded(false, "- omise token creation fail", null);
                    Timber.e("Omise token creation failed, code: " + errorCode);
                }
            });
        } catch (OmiseException e) {
            cardAddedListener.onCardAdded(false, "- requesting omise token failure", null);
            Timber.e(e, "Error requesting Token");
        }

    }

    public static void fetchAllCards(final CardFetchListener cardFetchListener) {
        ParseQuery<ParseObject> creditCardQuery = ParseQuery.getQuery("CreditCard");
        QueryUtils.addMatchCurrentUserToQuery(creditCardQuery, true);
        creditCardQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e != null) {
                    switch (e.getCode()) {
                        case 101: // No results found for query
                            Timber.d("No credit cards found");
                            break;
                        default:
                            Timber.e(e, "Error retrieving credit cards");
                    }
                } else {
                    cardFetchListener.onCardsFetched(objects);
                }
            }
        });
    }
    public static void getCard(String objectId, final CardFoundListener onCardFoundListener) {
        ParseQuery<ParseObject> cardQuery = ParseQuery.getQuery("CreditCard");
        cardQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e != null) {
                    Timber.e(e, "get Card failed");

                } else {
                    onCardFoundListener.onCardFound((FabCard) object);
                }
            }
        });
    }
    public interface CardFetchListener {
        void onCardsFetched(List<ParseObject> objects);
    }

    public interface CardAddedListener {
        void onCardAdded(Boolean resultSuccess, String errorCode, FabCard fabCard);
    }

    public interface CardFoundListener {
        void onCardFound(FabCard card);
    }
}

package com.fabbrigade.brigade.booking.model;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.widget.TextView;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.rating.model.Rating;
import com.fabbrigade.brigade.service.model.Service;
import com.fabbrigade.brigade.shared.model.FabUser;
import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Copyright FabBrigade 2015
 */
@ParseClassName("Booking")
public class Booking extends ParseObject {

    private static final String COLUMN_SERVICES = "services";
    private static final String COLUMN_ADDRESS = "address";
    private static final String COLUMN_PHONE_NUMBER = "phoneNumber";
    private static final String COLUMN_DATE = "date";
    private static final String COLUMN_CREDIT_CARD = "creditCard";
    private static final String COLUMN_NOTE = "note";
    private static final String COLUMN_BRIDGADETTE = "brigadette";
    private static final String COLUMN_RATING = "rating";
    private static final String COLUMN_CLIENT = "client";
    private static final String COLUMN_STATUS = "status";
    private static final String COLUMN_OMISE_CHARGE_ID = "omiseChargeId";
    private static final String COLUMN_APARTMENT = "apartment";
    private static final String COLUMN_CITY = "city";
    private static final String COLUMN_ZIP = "zip";

    public static final int STATUS_OPEN = 1;
    public static final int STATUS_CONFIRMED = 2;
    public static final int STATUS_STARTED = 3;
    public static final int STATUS_ENDED = 4;
    public static final int STATUS_CANCELLED_CUSTOMER = 10;
    public static final int STATUS_CANCELLED_BRIGADETTE = 11;
    public static final int STATUS_NOMATCH = 20;

    public List<Service> getServices() {
        return getList(COLUMN_SERVICES);
    }
    public void setRating(Rating rating) {
        if (rating != null) {
            put(COLUMN_RATING, rating);
        }
    }
    public Rating getRating() {
        return (Rating) get(COLUMN_RATING);
    }

    public void setStatusImage(Context ctx, TextView statusTextView) {
        int statusInt;
        int colorInt = R.color.booking_status_cancelled_unfulfilled;
        switch (getStatus()) {
            case STATUS_OPEN:
                statusInt = R.string.booking_status_open;
                colorInt = R.color.booking_status_open;
                break;
            case STATUS_CONFIRMED:
                statusInt = R.string.booking_status_confirmed;
                colorInt = R.color.booking_status_confirmed_completed;
                break;
            case STATUS_STARTED:
                statusInt = R.string.booking_status_started;
                colorInt = R.color.booking_status_confirmed_completed;
                break;
            case STATUS_ENDED:
                statusInt = R.string.booking_status_ended;
                colorInt = R.color.booking_status_confirmed_completed;
                break;
            case STATUS_CANCELLED_CUSTOMER:
                statusInt = R.string.booking_status_cancelled_customer;
                colorInt = R.color.booking_status_cancelled_unfulfilled;
                break;
            case STATUS_CANCELLED_BRIGADETTE:
                statusInt = R.string.booking_status_cancelled_brigadette;
                colorInt = R.color.booking_status_cancelled_unfulfilled;
                break;
            case STATUS_NOMATCH:
                statusInt = R.string.booking_status_nomatch;
                colorInt = R.color.booking_status_cancelled_unfulfilled;
                break;
            default:
                statusInt = R.string.booking_status_unknown;
        }
        statusTextView.setText(ctx.getString(statusInt));

        Drawable drawable = statusTextView.getBackground();
        if (drawable instanceof ShapeDrawable) {
            ((ShapeDrawable)drawable).getPaint().setColor(ctx.getResources().getColor(colorInt));
        } else if (drawable instanceof GradientDrawable) {
            ((GradientDrawable)drawable).setColor(ctx.getResources().getColor(colorInt));
        }
    }

    public void setServices(List<Service> services) {
        if (services != null) {
            put(COLUMN_SERVICES, services);
        }
    }

    public FabUser getBrigadette() {
        return (FabUser) get(COLUMN_BRIDGADETTE);
    }

    public void setClient(FabUser user) {
            if (user != null) {
                put(COLUMN_CLIENT, user);
            }
    }
    public int getStatus() {
        return getInt(COLUMN_STATUS);
    }
    public void setStatus(int status) {
        put(COLUMN_STATUS, status);
    }
    public String getAddress() {
        return getString(COLUMN_ADDRESS);
    }
    public void setAddress(String address) {
        if (address != null) {
            put(COLUMN_ADDRESS, address);
        }
    }
    public void setApartment(String apartment) {
        if (apartment != null) {
//            put(COLUMN_APARTMENT, apartment);
        }
    }
    public String getApartment() {
        return getString(COLUMN_APARTMENT);
    }
    public void setCity(String city) {
        if (city != null) {
            put(COLUMN_CITY, city);
        }
    }
    public String getCity() {
        return getString(COLUMN_CITY);
    }
    public void setPostalCode(String zip) {
        if (zip != null) {
//            put(COLUMN_ZIP, zip);
        }
    }
    public String getPostalCode() {
        return getString(COLUMN_ZIP);
    }

    public String getMailingAddressLine1() {
        String address = getAddress();
        String apartment = getApartment();
        if (apartment != null) {
            address = address + " " + apartment;
        }
        return  address;
    }
    public String getMailingAddressLine2() {
        return getCity() + ", " + getPostalCode();
    }

    public String getOmiseChargeId() {
        return getString(COLUMN_OMISE_CHARGE_ID);
    }
    public void setOmiseChargeId(String omiseChargeId) {
        if (omiseChargeId != null) {
            put(COLUMN_OMISE_CHARGE_ID, omiseChargeId);
        }
    }


    public String getPhoneNumber() {
        return getString(COLUMN_PHONE_NUMBER);
    }

    public void setPhoneNumber(String phone) {
        if (phone != null) {
            put(COLUMN_PHONE_NUMBER, phone);
        }
    }

    public Date getDate() {
        return getDate(COLUMN_DATE);
    }

    public void setDate(Date date) {
        if (date != null) {
            put(COLUMN_DATE, date);
        }
    }

    public FabCard getCreditCard() {
        return (FabCard) getParseObject(COLUMN_CREDIT_CARD);
    }

    public void setCreditCard(FabCard card) {
        if (card != null) {
            put(COLUMN_CREDIT_CARD, card);
        }
    }



    public String getNote() {
        return getString(COLUMN_NOTE);
    }

    public void setNote(String note) {
        if (note != null) {
            put(COLUMN_NOTE, note);
        }
    }


}

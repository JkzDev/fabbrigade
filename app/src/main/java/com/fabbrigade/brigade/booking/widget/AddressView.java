package com.fabbrigade.brigade.booking.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;

import com.fabbrigade.brigade.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Copyright FabBrigade 2015
 */
public class AddressView extends GridLayout implements View.OnFocusChangeListener {

    private Listener mListener;

    @Bind(R.id.street_address_1) EditText mAddressView1;
    @Bind(R.id.apartment_number) EditText mApartmentNumberView;
    @Bind(R.id.postal_code) EditText mPostalCodeView;
    @Bind(R.id.city) EditText mCityView;
    private Boolean clicked = false;

    public AddressView(Context context) {
        this(context, null);
    }

    public AddressView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AddressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = inflate(getContext(), R.layout.view_widget_address, this);
        ButterKnife.bind(view);

//        mAddressView1.setOnFocusChangeListener(this);
        mPostalCodeView.setOnFocusChangeListener(this);
//        mCityView.setOnFocusChangeListener(this);

//            mAddressView1.getEditText().setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mListener != null) {
//                        mListener.onClick(v);
//                    }
//                }
//            });

//        mAddressView1.addTextChangedListener(new SimpleTextWatcher());
//        mPostalCodeView.addTextChangedListener(new SimpleTextWatcher());
//        mCityView.addTextChangedListener(new SimpleTextWatcher());
//        mApartmentNumberView.addTextChangedListener(new SimpleTextWatcher());
    }



    public void setAddress1(String address) {
        mAddressView1.setText(address);
    }

    public void setApartmentNumber(String apartmentNumber) {
        mApartmentNumberView.setText(apartmentNumber);
    }

    public void setPostalCode(String postalCode) {
        mPostalCodeView.setText(postalCode);
    }

    public void setCity(String city) {
        mCityView.setText(city);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public boolean isValid() {
        return !mAddressView1.getText().toString().isEmpty();
    }

    public String getAddress1() {
        return mAddressView1 != null ? mAddressView1.getText().toString() : null;
    }

    public String getPostalCode() {
        return mPostalCodeView != null ? mPostalCodeView.getText().toString() : null;
    }
    public String getCity() {
        return mCityView != null ? mCityView.getText().toString() : null;
    }
    public String getApartment() {
        return mApartmentNumberView != null ? mApartmentNumberView.getText().toString() : null;
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!clicked && hasFocus) {
            mListener.onClick(v);
            clicked = true;
        }
    }

    public interface Listener {
        void onClick(View view);
        void onTextChanged(String text);
    }

    public class SimpleTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mListener != null) {
                mListener.onTextChanged(s.toString());
            }
        }
    }
}

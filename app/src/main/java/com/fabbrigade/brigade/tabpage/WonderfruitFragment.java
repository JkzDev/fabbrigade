//package com.fabbrigade.brigade.tabpage;
//
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ListView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.fabbrigade.brigade.R;
//
//import java.util.ArrayList;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//
///**
// * Created by Jakki3z on 12/8/15 AD.
// */
//public class WonderfruitFragment extends Fragment {
//
//    @Bind(R.id.listView)
//    ListView listView;
//    @Bind(R.id.booking_list_empty)
//    TextView bookingListEmpty;
//    @Bind(R.id.booking_list_loading)
//    ProgressBar bookingListLoading;
//    private int position;
//    public static ArrayList<wonderfruitVO> wonderfruitVO;
//
//    public WonderfruitFragment() {
//        super();
//    }
//
//    public WonderfruitFragment(int position, ArrayList<wonderfruitVO> wonderfruitVO) {
//        super();
//        this.position = position;
//        this.wonderfruitVO = wonderfruitVO;
//
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_listview, container, false);
//        ButterKnife.bind(this, view);
//        WonderfruitAdapter adapter = new WonderfruitAdapter(getContext(),R.layout.list_item_services, wonderfruitVO.get(position).getWonderfruitList(),wonderfruitVO.get(position).getCateId(),getActivity());
//        listView.setAdapter(adapter);
//        bookingListLoading.setVisibility(View.GONE);
//
//        return view;
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        ButterKnife.unbind(this);
//    }
//}
//
//

package com.fabbrigade.brigade.tabpage;

import java.util.ArrayList;

/**
 * Created by Jakki3z on 12/8/15 AD.
 */
public class wonderfruitVO {
    String CateId;
    String Catename;
    String Id;
    String description;
    String duration;
    String image;
    String image_small;
    String name;
    String price ;
    String serviceDescription;
    ArrayList<wonderfruitVO> wonderfruitList;



    public wonderfruitVO(
            String CateId,
            String Catename,
            ArrayList<wonderfruitVO> wonderfruitList
           ){

        this.CateId = CateId;
        this.Catename =Catename;
        this.wonderfruitList = wonderfruitList;

    }
    public wonderfruitVO(
    String Id,
    String description,
    String duration,
    String image,
    String image_small,
    String name,
    String price ,
    String serviceDescription){

        this.Id = Id;
        this.description = description;
        this.duration = duration;
        this.image = image;
        this.image_small = image_small;
        this.name = name;
        this.price =price;
        this.serviceDescription = serviceDescription;


    }


    public String getCateId() {
        return CateId;
    }

    public void setCateId(String cateId) {
        CateId = cateId;
    }

    public String getCatename() {
        return Catename;
    }

    public void setCatename(String catename) {
        Catename = catename;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_small() {
        return image_small;
    }

    public void setImage_small(String image_small) {
        this.image_small = image_small;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public ArrayList<wonderfruitVO> getWonderfruitList() {
        return wonderfruitList;
    }

    public void setWonderfruitList(ArrayList<wonderfruitVO> wonderfruitList) {
        this.wonderfruitList = wonderfruitList;
    }

}

package com.fabbrigade.brigade.tabpage;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.shared.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;


public class WonderfruitMapActivity extends BaseActivity implements View.OnClickListener {
    @Bind(R.id.parking)
    ImageView parking;
    @Bind(R.id.forbidden_fruit)
    ImageView forbiddenFruit;
    @Bind(R.id.the_ziggurat)
    ImageView theZiggurat;
    @Bind(R.id.theatre_of_feasts)
    ImageView theatreOfFeasts;
    @Bind(R.id.rocket_fruit)
    ImageView rocketFruit;
    @Bind(R.id.taste_of_wonder)
    ImageView tasteOfWonder;
    @Bind(R.id.wonder_salon)
    ImageView wonderSalon;

    @Bind(R.id.pavilion)
    ImageView pavilion;

    @Bind(R.id.goddess_camp)
    ImageView goddessCamp;
    @Bind(R.id.camp_wonder)
    ImageView campWonder;
    @Bind(R.id.boutique_camping)
    ImageView boutiqueCamping;
    @Bind(R.id.camping)
    ImageView camping;
    @Bind(R.id.play)
    ImageView play;

//
//    @Bind(R.id.img_map)
//    ZoomableImageView imgMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wonder_map_layout);
        ButterKnife.bind(this);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        Log.i("xy", "" + width + "\n" + height);


        parking.setOnClickListener(this);
        forbiddenFruit.setOnClickListener(this);
        theZiggurat.setOnClickListener(this);
        theatreOfFeasts.setOnClickListener(this);
        rocketFruit.setOnClickListener(this);
        tasteOfWonder.setOnClickListener(this);
        wonderSalon.setOnClickListener(this);
        pavilion.setOnClickListener(this);

        goddessCamp.setOnClickListener(this);
        campWonder.setOnClickListener(this);
        boutiqueCamping.setOnClickListener(this);
        play.setOnClickListener(this);
        camping.setOnClickListener(this);

    }


    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.parking:
                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Parking area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Parking";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;
            case R.id.forbidden_fruit:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Forbidden fruit area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Forbidden fruit";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();


                break;
            case R.id.the_ziggurat:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("The ziggurat area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "The ziggurat";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;
            case R.id.theatre_of_feasts:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Theatre of feasts area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Theatre of feasts";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;
            case R.id.rocket_fruit:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Rocket fruit area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Rocket fruit";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;
            case R.id.taste_of_wonder:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Taste of wonder area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Taste of wonder";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;
            case R.id.wonder_salon:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Wonder salon area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Wonder salon";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;

            case R.id.pavilion:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Pavilion area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Pavilion";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;

            case R.id.goddess_camp:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Goddess camp area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Goddess camp";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;
            case R.id.camp_wonder:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Camp wonder area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Camp wonder";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;
            case R.id.boutique_camping:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("Boutique camping area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "Boutique camping";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;

            case R.id.camping:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("General Camping  area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "General Camping";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;

            case R.id.play:


                new AlertDialogWrapper.Builder(this)
                        .setTitle("WONDERFRUIT")
                        .setMessage("General Camping area")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "General Camping";
                                finish();
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.map_name = "";
                            }
                        })
                        .show();
                break;


        }
    }
}

//package com.fabbrigade.brigade.tabpage;
//
//import android.app.Activity;
//import android.content.Context;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.bumptech.glide.Glide;
//import com.bumptech.glide.load.resource.drawable.GlideDrawable;
//import com.bumptech.glide.request.RequestListener;
//import com.bumptech.glide.request.target.Target;
//import com.fabbrigade.brigade.R;
//import com.fabbrigade.brigade.service.ServicesActivity;
//import com.fabbrigade.brigade.shared.StringUtils;
//import com.fabbrigade.brigade.shared.model.CartWonderfruit;
//
//import java.util.ArrayList;
//
///**
// * Created by Jakki3z on 12/9/15 AD.
// */
//public class WonderfruitAdapter extends ArrayAdapter<wonderfruitVO> {
//
//    Context context;
//    int resource;
//    ArrayList<wonderfruitVO> objects;
//Activity activity;
//    LayoutInflater inflater;
//    String gId;
//
//    public WonderfruitAdapter(Context context, int resource, ArrayList<wonderfruitVO> objects,String gId,Activity activity) {
//        super(context, resource, objects);
//        this.context = context;
//        this.resource = resource;
//        this.objects = objects;
//        this.gId = gId;
//        this.activity  = activity;
//        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//    }
//
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//
//        if(convertView ==null){
//            holder = new ViewHolder();
//            convertView = inflater.inflate(resource, parent, false);
//            holder.imageView = (ImageView) convertView.findViewById(R.id.imgView);
//            holder.name= (TextView) convertView.findViewById(R.id.name);
//            holder.description= (TextView) convertView.findViewById(R.id.description);
//            holder.duration= (TextView) convertView.findViewById(R.id.duration);
//            holder.contact= (TextView) convertView.findViewById(R.id.contact);
//            holder.bridal = (LinearLayout) convertView.findViewById(R.id.bridal);
//            holder.unbridal = (RelativeLayout) convertView.findViewById(R.id.unbridal);
//
//            holder.price= (TextView) convertView.findViewById(R.id.price);
//            holder.addServiceButton= (Button) convertView.findViewById(R.id.button_add_service);
//            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar);
//            convertView.setTag(holder);
//
//        }else{
//             holder = (ViewHolder)convertView.getTag();
//
//        }
//
//
//
//    Glide.with(context).load(objects.get(position).getImage())
//
//            .listener(new RequestListener<String, GlideDrawable>() {
//                @Override
//                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                    return false;
//                }
//
//                @Override
//                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                    holder.progressBar.setVisibility(View.GONE);
//                    return false;
//                }
//            })
//            .placeholder(R.drawable.placholder)
//            .into(holder.imageView);
//
//if(objects.size()>1){
//    holder.name.setText(objects.get(position).getName());
//    if (TextUtils.isEmpty(objects.get(position).getDescription())) {
//        holder.description.setVisibility(View.GONE);
//    } else {
//        holder.description.setVisibility(View.VISIBLE);
//        holder.description.setText(objects.get(position).getDescription());
//    }
//    if(objects.get(position).getDuration().toString().length()!=0){
//        holder.duration.setText(StringUtils.getTimeString(getContext(), Integer.parseInt(objects.get(position).getDuration())));
//
//
//    }
//
//    if(objects.get(position).getPrice().toString().length()!=0){
//        holder.price.setText(StringUtils.getCurrencyString(getContext(), Integer.parseInt(objects.get(position).getPrice())));
//
//    }
//
//
//    holder.addServiceButton.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            addOrRemoveServiceToCart(objects.get(position));
//            setButtonText(holder, objects.get(position));
//            Log.i("TAG",""+objects.get(position).getName()+"\nGid = "+gId);
//        }
//    });
//
//}else{
//    holder.name.setText(objects.get(position).getName());
//    String[] dis;
//    String[] tel;
//
//    if (TextUtils.isEmpty(objects.get(position).getDescription())) {
//
//
//        holder.description.setVisibility(View.GONE);
//    } else {
//        dis = objects.get(position).getDescription().split("&&&");
//        tel = dis[1].split(" | ");
//
//        holder.description.setVisibility(View.VISIBLE);
//        holder.description.setText(dis[0]);
//
//        holder.bridal.setVisibility(View.VISIBLE);
//        holder.unbridal.setVisibility(View.GONE);
//
//    }
//}
//
//
//
//
////    holder.addServiceButton.setVisibility(View.GONE);
//
//
//
//
//
//
//
////
//        setButtonText(holder, objects.get(position));
//
//
//
//
//
//
//        return convertView;
//    }
//
//    public void setServices(ArrayList<wonderfruitVO> services) {
//        objects = services;
//        notifyDataSetChanged();
//    }
//
//    private void addOrRemoveServiceToCart(wonderfruitVO service) {
//        CartWonderfruit cart = ((ServicesActivity)activity).getCartW();
//        if (cart.containsService(service)) { // TODO animate button for less jarring fade (also make flat)
////            service.unpinInBackground();
//            cart.removeServiceFromCart(service, activity);
//        } else {
////            service.pinInBackground();
//            cart.addServiceToCart(service, activity);
//        }
//    }
//
//    private void setButtonText(ViewHolder holder, wonderfruitVO service) {
//        CartWonderfruit cart = ((ServicesActivity) activity).getCartW();
//        if (cart.containsService(service)) {
//            holder.addServiceButton.setText(R.string.button_remove_service);
//        } else {
//            holder.addServiceButton.setText(R.string.button_book_service);
//        }
//    }
//
//
//
//    @Override
//    public int getCount() {
//        return objects.size();
//    }
//
//
//
//    static class ViewHolder {
//        ImageView imageView;
//        TextView name;
//        TextView description;
//        TextView duration;
//        TextView contact;
//        TextView price;
//        LinearLayout bridal;
//        RelativeLayout unbridal;
//
//        Button addServiceButton;
//        ProgressBar progressBar;
//    }
//
//
//}

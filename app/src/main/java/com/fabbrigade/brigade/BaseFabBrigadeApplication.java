package com.fabbrigade.brigade;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.booking.model.FabCard;
import com.fabbrigade.brigade.category.model.Category;
import com.fabbrigade.brigade.rating.model.Rating;
import com.fabbrigade.brigade.service.model.Service;
import com.fabbrigade.brigade.shared.model.FabUser;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseUser;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Copyright FabBrigade 2015
 */
public abstract class BaseFabBrigadeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        // logging setup
        Timber.plant(new LoggingTree(this));

        // Glide setup
        // TODO:set up disk cache

        // parse setup
        ParseObject.registerSubclass(Category.class);
        ParseObject.registerSubclass(Service.class);
        ParseObject.registerSubclass(Booking.class);
        ParseObject.registerSubclass(FabCard.class);
        ParseObject.registerSubclass(Rating.class);
        ParseUser.registerSubclass(FabUser.class);

        Parse.enableLocalDatastore(this); //TODO unpin all on destroy


//        Server Parse
        Parse.initialize(this, "es6YcnjCesANK51tLds7hfdKUxzRMs1xOJRa0oVE", "H7bdg2qbugZj7ztLyAxfe1dFRBiHCaQ4uCel8JDv");

//        Server Clone
//        Parse.initialize(this, "x7v8X1GNWz66fpjSXj0sKHoJ1pDgqRTH4G7hVzfW", "GItjegsPIZuE3HlLRxRWMgmKki0UgQVJsYGUAgRY");


        ParseFacebookUtils.initialize(this);

//        // parse anonymous user setup
//        ParseUser.enableAutomaticUser();
//
//        final ParseUser currentUser = ParseUser.getCurrentUser();
//        currentUser.saveInBackground();

        // set up installation for push
//        ParseInstallation.getCurrentInstallation().put("user", ParseUser.getCurrentUser());
//        ParseInstallation.getCurrentInstallation().saveInBackground();



    }


}

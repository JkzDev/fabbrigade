package com.fabbrigade.brigade.category.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

/**
 * Copyright FabBrigade 2015
 */
@ParseClassName("Category")
public class Category extends ParseObject {
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_DESCRIPTION = "description";
    private static final String COLUMN_IMAGE = "image";
    private static final String COLUMN_ENABLED = "enabled";
    private static final String COLUMN_PARENT = "parent";

    public String getName() {
        return getString(COLUMN_NAME);
    }

    public String getDescription() {
        return getString(COLUMN_DESCRIPTION);
    }

    public ParseFile getImage() {
        return getParseFile(COLUMN_IMAGE);
    }
    public Category getParent() {
        return (Category) get(COLUMN_PARENT);
    }
    public void setParent(Category category) {
        if (category != null) {
            put(COLUMN_PARENT, category);
        }
    }
    public boolean isEnabled() {
        return getBoolean(COLUMN_ENABLED);
    }


    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Category)) {
            return false;
        }

        return ((Category) o).getObjectId().equals(getObjectId());
    }

}

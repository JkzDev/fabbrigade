package com.fabbrigade.brigade.category;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.category.model.Category;
import com.fabbrigade.brigade.service.ServicesActivity;
import com.fabbrigade.brigade.shared.BaseFragment;
import com.fabbrigade.brigade.shared.ViewUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Copyright FabBrigade 2015
 */
public class CategoriesFragment extends BaseFragment {

    @Bind(R.id.booking_list)
    RecyclerView mList;

    private int mItemHeight = 0;

    private List<Category> categoryList;

    private OnServiceClickListener mServiceClickListener = new OnServiceClickListener() {
        @Override
        public void onServiceClick(String id) {
            getActivity().startActivityForResult(ServicesActivity.getIntent(getActivity(), id), MainActivity.RC_BOOKING);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        ButterKnife.bind(this, view);
        ButterKnife.findById(view, R.id.booking_list_loading).setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mItemHeight = (int) Math.ceil(ViewUtils.getContentHeight(getActivity()) *170/320);
        categoryList = new ArrayList<>();
        mList.setAdapter(new CategoriesAdapter(categoryList, mServiceClickListener));
        mList.setLayoutManager(new LinearLayoutManager(getContext()));
        new ParseQuery<>(Category.class)
                .orderByAscending("position")
                .whereEqualTo("parent", null)
                .findInBackground(new FindCallback<Category>() {
                    @Override
                    public void done(final List<Category> categories, ParseException e) {
                        if (e != null) {
                            Timber.e(e, "Error fetching categories.");
                            return;
                        }
                        categoryList.clear();
                        categoryList.addAll(categories);
                        mList.getAdapter().notifyItemRangeInserted(0, categoryList.size());
                    }
                });
    }


    private class CategoriesAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

        private List<Category> mCategories;
        private OnServiceClickListener mListener;

        public CategoriesAdapter(List<Category> categories, OnServiceClickListener listener) {
            mCategories = categories;
            mListener = listener;
        }

        @Override
        public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_categories, parent, false);
//            final ViewGroup.LayoutParams lp = view.getLayoutParams();
//            lp.height = mItemHeight;
//            view.setLayoutParams(lp);
            return new CategoryViewHolder(view);
        }

        @Override
        public void onBindViewHolder(CategoryViewHolder holder, int position) {
            final Category category = mCategories.get(position);
            if (category.getImage() != null) {
                Glide.with(holder.itemView.getContext())
                        .load(category.getImage().getUrl())
                        .fitCenter()
                        .dontAnimate()
                        .into(holder.image);
            }
            if (category.isEnabled()) {

//                holder.book.setVisibility(View.VISIBLE);
                if(position!=2){
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mListener.onServiceClick(category.getObjectId());
                            Log.i("OnClick book", category.getObjectId());
                        }
                    });
                }else{
                    holder.book.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            mListener.onServiceClick(category.getObjectId());
                            Log.i("OnClick book", category.getObjectId());
                        }
                    });
                }


            } else {

//                holder.book.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return mCategories == null ? 0 : mCategories.size();
        }
    }

    protected class CategoryViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.category_image)
        ImageView image;

        @Bind(R.id.button_book)
        Button book;

        public CategoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    protected interface OnServiceClickListener {
        void onServiceClick(String id);
    }
}

package com.fabbrigade.brigade.category;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.me.MeActivity;
import com.fabbrigade.brigade.profile.LoginSplashActivity;
import com.fabbrigade.brigade.profile.ProfileActionView;
import com.fabbrigade.brigade.rating.RatingFragment;
import com.fabbrigade.brigade.service.ServicesActivity;
import com.fabbrigade.brigade.shared.BaseActivity;
import com.fabbrigade.brigade.shared.QueryUtils;
import com.fabbrigade.brigade.walkthrough.WalkthroughActivity;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class MainActivity extends BaseActivity {

    private static int RC_SPLASH = 664;
    public static int RC_BOOKING = 668;
    @Bind(R.id.wonder)
    ImageView wonder;
    @Bind(R.id.glam)
    ImageView glam;
    @Bind(R.id.spa)
    ImageView spa;

    private ProfileTracker mProfileTracker;

    PackageInfo info;
    String deviceVersion;
    ProfileActionView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setLogo(R.drawable.fabbrigade_logo);
            actionBar.setDisplayUseLogoEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        try {
            info = getPackageManager().getPackageInfo("com.fabbrigade.brigade", PackageManager.GET_SIGNATURES);
            deviceVersion = info.versionName;
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        wonder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.from_menu = 0;
                Utils.categoryId = "43cL7q0cR6";
                startActivityForResult(ServicesActivity.getIntent(MainActivity.this, "43cL7q0cR6"), MainActivity.RC_BOOKING);

            }
        });
        glam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.from_menu = 1;
                Utils.categoryId =  "ZLGoyRPbWB";
                startActivityForResult(ServicesActivity.getIntent(MainActivity.this, "ZLGoyRPbWB"), MainActivity.RC_BOOKING);
            }
        });


//


//
//
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.container, new CategoriesFragment())
//                .commit();
//        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Timber.d("success");
//            }
//
//            @Override
//            public void onCancel() {
//                Timber.d("cancel");
//            }
//
//            @Override
//            public void onError(FacebookException e) {
//                Timber.d("error");
//            }
//        });

        ParseUser user = ParseUser.getCurrentUser();
        if (user != null) {
            checkForPendingReview();
            checkForProfile();
        }

        checkForWalkthrough(false);
    }

    private void checkForProfile() {
        if (Profile.getCurrentProfile() == null) {
            mProfileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                    if (profile2 != null) {
                        Log.v("facebook - profile", profile2.getFirstName());
                        mProfileTracker.stopTracking();
                    }
                }
            };
            mProfileTracker.startTracking();

            Collection<String> permissions = new ArrayList<>();
            permissions.add("email");
            permissions.add("public_profile");
            ParseFacebookUtils.logInWithReadPermissionsInBackground(MainActivity.this, permissions, new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if (e != null) {
                        Timber.e(e, "Problem logging fb in");
                    } else {
                        invalidateOptionsMenu();
                    }

                }
            });


        } else {
            Profile profile = Profile.getCurrentProfile();
            Log.v("facebook - profile", profile.getFirstName());
        }
    }

    public void checkForWalkthrough(boolean force) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (force || !prefs.getBoolean(WalkthroughActivity.TUTORIAL_FINISHED, false)) {
            Intent i = new Intent(MainActivity.this, WalkthroughActivity.class);
            startActivity(i);
        }
    }

    public void checkForPendingReview() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Booking");
        query.whereEqualTo("status", 4);
        query.whereDoesNotExist("rating");

        QueryUtils.addMatchCurrentUserToQuery(query, false);

        query.include("brigadette");
        query.include("services");
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(final ParseObject object, ParseException e) {
                if (e != null) {
                    switch (e.getCode()) {
                        case 101: // No results found for query
                            Timber.d("No pending reviews");
                            break;
                        default:
                            Timber.e(e, "Error retrieving booking for pending review");
                    }
                }
                if (object != null) {

                    object.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e != null) {
                                Timber.e(e, "Failed to save booking");
                            } else {
                                RatingFragment ratingFragment = RatingFragment.newInstance((Booking) object);
                                ratingFragment.setCancelable(false);
                                ratingFragment.show(getFragmentManager(), getClass().getSimpleName());
                            }

                        }
                    });

                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_profile).setActionView(new ProfileActionView(this));

        view = (ProfileActionView) MenuItemCompat.getActionView(menu.findItem(R.id.action_profile));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser user = ParseUser.getCurrentUser();
                if (user == null) {
                    startActivityForResult(new Intent(MainActivity.this, LoginSplashActivity.class), RC_SPLASH);
                } else {
                    startActivity(new Intent(MainActivity.this, MeActivity.class));
                }
            }
        });

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SPLASH) {
            if (resultCode == RESULT_OK) {
                invalidateOptionsMenu();
            }
        } else if (requestCode == RC_BOOKING) {
            invalidateOptionsMenu();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        view = (ProfileActionView) MenuItemCompat.getActionView(menu.findItem(R.id.action_profile));

        menu.findItem(R.id.action_profile).setVisible(true);
        if (Profile.getCurrentProfile() != null) {
            view.setProfile(Profile.getCurrentProfile());
        }

        return true;
    }


    @Override
    public void onResume() {
        super.onResume();

//        ParseUser user = ParseUser.getCurrentUser();
//        if (user == null) {
//            if (view != null) {
//                ProfileActionView.mIcon.setImageResource(R.drawable.clear);
//                ParseUser.logOut();
//            }
//
//        } else {
//            if (view != null) {
//                view.setProfile(Profile.getCurrentProfile());
//            }
//
//
//        }

    }


}

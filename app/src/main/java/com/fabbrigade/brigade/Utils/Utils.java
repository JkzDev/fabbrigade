package com.fabbrigade.brigade.Utils;

import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.tabpage.wonderfruitVO;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Jakki3z on 12/9/15 AD.
 */
public class Utils {

    public static final int STATUS_OPEN = 1;
    public static final int STATUS_CONFIRMED = 2;
    public static final int STATUS_STARTED = 3;
    public static final int STATUS_ENDED = 4;
    public static final int STATUS_CANCELLED_CUSTOMER = 10;
    public static final int STATUS_CANCELLED_BRIGADETTE = 11;
    public static final int STATUS_NOMATCH = 20;

    public static int cart_total = 0;
    public static String map_name = "";
    public static ArrayList<wonderfruitVO> wonderfruit;
    public static String categoryId ;
    public static String Email ;
    public static int from_menu = 0;
    public static Booking booking;
    public static boolean isFacebookLogin = false;

    //book
    public static String totalDulation;
    public static String totalPrice;


    //date
    public static String date;
    public static String time;






    public static final String Url_HISTORY = "http://aplaydigital.com/home/fabb/api.php?type=bookingHisty&user_email=%s";

    public static final String HISTORY_DETAIL = "http://aplaydigital.com/home/fabb/api.php?type=bookingDetail&id=%s";


    public static final String HISTORY_CANCLE = "http://aplaydigital.com/home/fabb/api.php?type=cancel&id=%s";





//
public static final String getCancleURL(String id){
    String url = String.format(Utils.HISTORY_CANCLE,id);
    return url;
}


    public static final String getHistoryURL(String email){
        String url = String.format(Utils.Url_HISTORY,email);
        return url;
    }


    public static final String getHistoryDetail(String id){
        String url = String.format(Utils.HISTORY_DETAIL,id);
        return url;
    }


    public static String OKHttpPostData(String url){
        String _response;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                _response = response.body().string();
            } else {
                _response = "Not Success - code : " + response.code();
            }
        } catch (IOException e) {
            e.printStackTrace();
            _response = "Error - " + e.getMessage();
        }

        return _response;
    }

    public static String PostBookData(String address,String price,String booking_dt,String totaldulation,String booking_detail ,
                                      String user_name,String user_email,String user_phone){

        String url = "http://aplaydigital.com/home/fabb/api.php?";

        String _response;
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormEncodingBuilder()
                .add("type", "booking")
                .add("address", address)
                .add("price", price)
                .add("booking_dt", booking_dt)
                .add("total", totaldulation)
                .add("booking_detail", booking_detail)
                .add("user_name", user_name)
                .add("user_email", user_email)
                .add("user_phone", user_phone)
                .add("mobile_type", "AD")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                _response = response.body().string();
            } else {
                _response = "Not Success - code : " + response.code();
            }
        } catch (IOException e) {
            e.printStackTrace();
            _response = "Error - " + e.getMessage();
        }

        return _response;
    }

}




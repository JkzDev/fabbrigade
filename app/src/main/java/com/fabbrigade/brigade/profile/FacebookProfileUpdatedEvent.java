package com.fabbrigade.brigade.profile;

import com.facebook.Profile;

/**
 * Copyright FabBrigade 2015.
 */
public class FacebookProfileUpdatedEvent {
    private final Profile mProfile;

    public FacebookProfileUpdatedEvent(Profile profile) {
        mProfile = profile;
    }

    public Profile getProfile() {
        return mProfile;
    }
}

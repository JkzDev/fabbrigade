package com.fabbrigade.brigade.profile;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.facebook.Profile;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Copyright FabBrigade 2015
 */
public class ProfileActionView extends LinearLayout implements View.OnClickListener {

    public static ImageView mIcon;


    public ProfileActionView(Context context) {
        super(context);
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setGravity(Gravity.CENTER_VERTICAL);
        setClickable(true);

        final int padding = getContext().getResources().getDimensionPixelSize(R.dimen.activity_vertical_margin);
        setPadding(padding, padding, padding, padding);

        final int size = getContext().getResources().getDimensionPixelSize(R.dimen.action_icon_size);
        mIcon = new ImageView(getContext());
        mIcon.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, size));
        mIcon.setImageResource(R.drawable.clear);
        mIcon.setScaleType(ImageView.ScaleType.FIT_END);
//        mIcon.setAdjustViewBounds(true);
        addView(mIcon);
    }

    public void setProfile(Profile profile) {
        final int size = getContext().getResources().getDimensionPixelSize(R.dimen.action_icon_profile_size);

        if( Utils.isFacebookLogin){
            Glide.with(getContext())
                    .load(profile.getProfilePictureUri(size, size))
                    .dontAnimate()
                    .bitmapTransform(new CropCircleTransformation(getContext()))
                    .into(mIcon);

        }


    }

    @Override
    public void onClick(View v) {

    }
}

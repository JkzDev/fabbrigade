package com.fabbrigade.brigade.profile;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.shared.BaseActivity;
import com.fabbrigade.brigade.shared.model.EmailUser;
import com.fabbrigade.brigade.shared.model.FabUser;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

import static butterknife.ButterKnife.findById;

public class LoginSplashActivity extends BaseActivity implements View.OnClickListener {


    @Bind(R.id.email_login)
    EditText emailLogin;
    @Bind(R.id.password_login)
    EditText passwordLogin;
    @Bind(R.id.fpassword_login)
    EditText fpasswordLogin;
    @Bind(R.id.fpassword)
    TextView fpassword;
    @Bind(R.id.login_facebook)
    ImageView loginFacebook;
    @Bind(R.id.login_email)
    TextView loginEmail;
    @Bind(R.id.create_account)
    TextView createAccount;

     public int STAGE = 0;


     LoginButton loginButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_splash);
        ButterKnife.bind(this);

        // setup actionbar
        final Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        }
        loginFacebook.setOnClickListener(this);
        loginEmail.setOnClickListener(this);
        createAccount.setOnClickListener(this);







        // setup facebook login button
       loginButton = findById(this, R.id.login_button);
        loginButton.setReadPermissions("public_profile", "email");

        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                Timber.d("Facebook login succeeded");

                Collection<String> permissions = new ArrayList<>();
                permissions.add("email");
                permissions.add("public_profile");
                ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginSplashActivity.this, permissions, new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException e) {
                        if (e != null) {
                            Timber.e(e, "Problem logging fb in");
                        } else {

                            Utils.isFacebookLogin = true;
                            ParseInstallation.getCurrentInstallation().put("user", ParseUser.getCurrentUser());
                            ParseInstallation.getCurrentInstallation().saveInBackground();
                            FabUser.saveFBDetails();
                            setResult(RESULT_OK);
                            finish();

                        }

                    }
                });

            }

            @Override
            public void onCancel() {
                Timber.d("Facebook login cancelled");
            }

            @Override
            public void onError(FacebookException e) {
                Timber.e(e, "Facebook login failed");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_facebook :
                loginButton.performClick();
                break;

            case R.id.create_account :
                if(STAGE==0){
                    STAGE=1;
                    fpassword.setVisibility(View.GONE);
                    fpasswordLogin.setVisibility(View.VISIBLE);
                    createAccount.setText("Back to login");
                    loginEmail.setText("Register");


                }else{
                    STAGE=0;
                    fpassword.setVisibility(View.VISIBLE);
                    fpasswordLogin.setVisibility(View.GONE);
                    createAccount.setText("I don't have an account");
                    loginEmail.setText("LOGIN");

                }


                break;



            case R.id.login_email :

                if(STAGE==0){

                    if(passwordLogin.getText().toString().length()==0||emailLogin.getText().toString().length()==0){
                        Toast.makeText(this,"Plese insert complete fielde",Toast.LENGTH_LONG).show();
                    }else{
                        if(passwordLogin.getText().toString().length()<4){
                            Toast.makeText(this,"You password must be  4 character or more!!",Toast.LENGTH_LONG).show();
                            passwordLogin.setText("");
                        }else{

                            ParseUser.logInInBackground(emailLogin.getText().toString(), passwordLogin.getText().toString(), new LogInCallback() {
                                public void done(ParseUser user, ParseException e) {
                                    if (user != null) {
                                        Bundle parameters = new Bundle();
                                        parameters.putString("fields", "email");
                                        setResult(RESULT_OK);
                                        finish();
                                    } else {
                                        Toast.makeText(LoginSplashActivity.this,"Please Tryagain",Toast.LENGTH_LONG).show();
                                        passwordLogin.setText("");
                                        emailLogin.setText("");
                                    }
                                }
                            });
                        }



                    }



                }
                else if(STAGE==1){
                    if(passwordLogin.getText().toString().equalsIgnoreCase(fpasswordLogin.getText().toString())){
                        if(passwordLogin.getText().toString().length()==0||emailLogin.getText().toString().length()==0||fpasswordLogin.getText().length()==0){
                            Toast.makeText(this,"Plese insert complete fielde",Toast.LENGTH_LONG).show();

                        }else{

//                        LoginWithEmail(emailLogin.getText().toString(),emailLogin.getText().toString(),emailLogin.getText().toString());
                            FabUser user = new FabUser();


                            user.setUsername(emailLogin.getText().toString());
                            user.setPassword(passwordLogin.getText().toString());
                            user.setEmail(emailLogin.getText().toString());

// other fields can be set just like with ParseObject

                            user.signUpInBackground(new SignUpCallback() {
                                public void done(ParseException e) {
                                    if (e == null) {
                                        // Hooray! Let them use the app now.



                                        ParseUser.logInInBackground(emailLogin.getText().toString(), passwordLogin.getText().toString(), new LogInCallback() {
                                            public void done(ParseUser user, ParseException e) {
                                                if (user != null) {
                                                    Bundle parameters = new Bundle();
                                                    parameters.putString("fields", "email");
                                                    setResult(RESULT_OK);
                                                    finish();
                                                } else {
                                                    // Signup failed. Look at the ParseException to see what happened.
                                                }
                                            }
                                        });


                                    } else {
                                        // Sign up didn't succeed. Look at the ParseException
                                        // to figure out what went wrong
                                    }
                                }
                            });



                        }
                    }else{
                        Toast.makeText(this,"You password and confirm password Not math!!",Toast.LENGTH_LONG).show();
                        passwordLogin.setText("");
                        fpasswordLogin.setText("");

                    }



                }




                break;




        }
    }

    public void LoginWithEmail(String name,String Email,String email){
        ParseInstallation.getCurrentInstallation().saveInBackground();
        EmailUser.saveFBDetails(name,Email,email);


        setResult(RESULT_OK);
        finish();
    }

}

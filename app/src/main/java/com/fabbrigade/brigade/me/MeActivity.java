package com.fabbrigade.brigade.me;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.bumptech.glide.Glide;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.shared.BaseActivity;
import com.fabbrigade.brigade.shared.model.FabUser;
import com.facebook.Profile;
import com.facebook.login.widget.LoginButton;
import com.parse.ParseUser;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static butterknife.ButterKnife.findById;

/**
 * Created by Laptop on 10/25/2015.
 */
public class MeActivity extends BaseActivity {

    @Bind(R.id.profile_image)
    ImageView profileImage;
    @Bind(R.id.pager)
    ViewPager pager;
    @Bind(R.id.signout)
    Button signout;
    @Bind(R.id.me_listview)
    ListView meListview;
    ArrayList<MeVO> meList;
    @Bind(R.id.progress)
    ProgressBar progress;
    @Bind(R.id.layoutView)
    LinearLayout layoutView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        ButterKnife.bind(this);

        final Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        FabUser user = (FabUser) ParseUser.getCurrentUser();
        meList = new ArrayList<>();


        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(user.getFirstName(this));
        }
        int size = (int) getResources().getDimension(R.dimen.profile_image_dimen);
        Profile profile = Profile.getCurrentProfile();
        if (profile.getProfilePictureUri(size, size) != null) {
            Glide.with(this)
                    .load(profile.getProfilePictureUri(size, size))
                    .dontAnimate()
                    .bitmapTransform(new CropCircleTransformation(this))
                    .into(profileImage);
        }

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginButton loginButton = findById(MeActivity.this, R.id.login_button);
                if (Utils.isFacebookLogin) {
                    ParseUser.logOut();
                    loginButton.performClick();
                    finish();
                } else {
                    new AlertDialogWrapper.Builder(MeActivity.this)
                            .setTitle("FabBrigade")
                            .setMessage("Please confirm logout?")
                            .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    ParseUser.logOut();
                                    finish();
                                }
                            })
                            .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();

                }
            }
        });

        TextView name = findById(this, R.id.me_name);
        name.setText(user.getFullName(this));

        TextView email = findById(this, R.id.me_email);
        email.setText(user.getEmail(this));


        getData(Utils.getHistoryURL(user.getEmail(this)));

        meListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(MeActivity.this,"Position"+meList.get(position).getId(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MeActivity.this, MyHistoryDetail.class);
                intent.putExtra("Id", meList.get(position).getId());

                startActivity(intent);


            }
        });


//        TabLayout tabLayout = findById(this, R.id.tabs);
//        pager = findById(this, R.id.pager);
//        pager.setAdapter(new BookingsPagerAdapter(getSupportFragmentManager()));
//        tabLayout.setupWithViewPager(pager);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class BookingsPagerAdapter extends FragmentPagerAdapter {

        public BookingsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt(MeBookingsFragment.ARG_MODE, position);
            Fragment fragment = new MeBookingsFragment();
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return MeBookingsFragment.getTitle(position, getApplicationContext());
        }
    }


    public void getData(final String url) {
        progress.setVisibility(View.VISIBLE);
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                OkHttpClient okHttpClient = new OkHttpClient();
                String data = Utils.OKHttpPostData(url);
                return data;
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                try {
                    progress.setVisibility(View.GONE);
                    layoutView.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = new JSONObject(string);
                    if (jsonObject.getString("status").equalsIgnoreCase("200")) {
                        JSONArray JArray = jsonObject.getJSONArray("item");
                        for (int i = 0; i < JArray.length(); i++) {
                            JSONObject dataObject = JArray.getJSONObject(i);

                            meList.add(new MeVO(
                                    dataObject.getString("id"),
                                    dataObject.getString("address"),
                                    dataObject.getString("booking_dt")));
                        }
                        MeAdapter adapter = new MeAdapter(MeActivity.this, meList);
                        meListview.setAdapter(adapter);

                    } else {


                        return;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }


            }
        }.execute();

    }


}

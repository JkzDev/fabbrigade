package com.fabbrigade.brigade.me;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.Utils.Utils;
import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.shared.BaseActivity;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Jakki3z on 12/16/15 AD.
 */
public class MyHistoryDetail extends BaseActivity implements View.OnClickListener{

    @Bind(R.id.booking_detail_address1) TextView address1;
    @Bind(R.id.booking_detail_address2) TextView address2;
    @Bind(R.id.booking_detail_date1) TextView date1;
    @Bind(R.id.booking_detail_date2) TextView date2;
    @Bind(R.id.booking_detail_services) TextView services;
    @Bind(R.id.booking_detail_cost) TextView cost;
    @Bind(R.id.booking_detail_payment) TextView payment;
    @Bind(R.id.booking_detail_booking_ref) TextView bookingRef;
    @Bind(R.id.booking_detail_brigadette) TextView brigadette;
    @Bind(R.id.booking_detail_brigadette_block) LinearLayout brigadetteBlock;
    @Bind(R.id.booking_detail_status) TextView status;
    @Bind(R.id.booking_details_rate_bar) RatingBar ratingBar;
    @Bind(R.id.booking_details_confirmation_description) TextView confirmationDescription;
    @Bind(R.id.booking_details_image0) ImageView serviceImage0;
    @Bind(R.id.booking_details_image1) ImageView serviceImage1;
    @Bind(R.id.booking_details_image2) ImageView serviceImage2;
    @Bind(R.id.booking_details_image3) ImageView serviceImage3;
    @Bind(R.id.booking_details_image4) ImageView serviceImage4;

    @Bind(R.id.booking_detail_cancel_block) LinearLayout cancelBlock;
    @Bind(R.id.booking_detail_cancel_booking) TextView cancelBooking;

    @Bind(R.id.service_block)
    RelativeLayout servicesBlock;
    @Bind(R.id.booking_details_widget_block) LinearLayout serviceDetailsBlock;
    @Bind(R.id.booking_details_service_expand_button) ImageView serviceExpandButton;




    @Bind(R.id.progressDetail)
    ProgressBar progress;

    @Bind(R.id.addView)
    LinearLayout addView;


    @Bind(R.id.layoutView)
    ScrollView layoutView;



    private boolean SERVICES_EXPANDED = false;

    private static final String DATE1_FORMAT = "EEEE, MMMM d";
    private static final String DATE2_FORMAT = "h:mma";

    String id;
    JSONArray dataArray;
    JSONObject itemObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_booking_details);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        id = extras.getString("Id");


        getData(Utils.getHistoryDetail(id));
        servicesBlock.setOnClickListener(this);
        cancelBooking.setOnClickListener(this);

    }
    public void setData(JSONObject data){
        dataArray = new JSONArray();
        try {
            setViewsBasedOnStatus(data);
            setStatusImage(data,status);

            address1.setText(data.getString("address"));
            date1.setText(data.getString("booking_dt"));
            services.setText(data.getString("address"));
            cost.setText(data.getString("price"));

            dataArray = data.getJSONArray("booking_detail");
            for (int i = 0; i <dataArray.length() ; i++) {
                JSONObject dataObject = dataArray.getJSONObject(i);
                insertView(dataObject,i);
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }



    }






    public void getData(final String url){
        progress.setVisibility(View.VISIBLE);
        itemObject = new JSONObject();
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                OkHttpClient okHttpClient = new OkHttpClient();
                String data = Utils.OKHttpPostData(url);
                return data;
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                try {progress.setVisibility(View.GONE);
                    layoutView.setVisibility(View.VISIBLE);

                    JSONObject jsonObject  = new JSONObject(string);
                    if(jsonObject.getString("status").equalsIgnoreCase("200")){

                        itemObject = jsonObject.getJSONObject("item");
                        setData(itemObject);

                    }else{


                        return;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }


            }
        }.execute();

    }


    public void setViewsBasedOnStatus(JSONObject data) throws JSONException {
        switch (Integer.parseInt(data.getString("status"))) {
            case Booking.STATUS_OPEN:
                brigadetteBlock.setVisibility(View.GONE);
                ratingBar.setVisibility(View.GONE);
                break;
            case Booking.STATUS_CONFIRMED:
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                break;
            case Booking.STATUS_STARTED:
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                break;
            case Booking.STATUS_ENDED:
                confirmationDescription.setVisibility(View.GONE);
                cancelBlock.setVisibility(View.GONE);
                break;
            case Booking.STATUS_CANCELLED_BRIGADETTE:
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                cancelBlock.setVisibility(View.GONE);
                break;
            case Booking.STATUS_CANCELLED_CUSTOMER:
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                cancelBlock.setVisibility(View.GONE);
                break;
            case Booking.STATUS_NOMATCH:
                brigadetteBlock.setVisibility(View.GONE);
                ratingBar.setVisibility(View.GONE);
                confirmationDescription.setVisibility(View.GONE);
                cancelBlock.setVisibility(View.GONE);
                break;
        }
    }

    private void insertView(JSONObject data, int index) throws JSONException {
        View layout2 = LayoutInflater.from(MyHistoryDetail.this).inflate(R.layout.list_item_booking_details_service, addView, false);
        ImageView imageView = (ImageView) layout2.findViewById(R.id.item_service_image);
        TextView text1 = (TextView) layout2.findViewById(R.id.item_service_text1);
        TextView text2 = (TextView) layout2.findViewById(R.id.item_service_text2);

        text1.setText(data.getString("name"));
        String price = data.getString("price");
        long duration = Long.parseLong(data.getString("time"));
        if (duration < 60) {
            text2.setText(MyHistoryDetail.this.getString(
                    R.string.booking_details_service_summary_no_hours,
                    String.valueOf(duration), price));
        } else {
            long hours = duration / 60;
            long minutes = duration - (60*hours);

            if (minutes == 0) {
                text2.setText(MyHistoryDetail.this.getString(
                        R.string.booking_details_service_summary_no_minutes,
                        String.valueOf(hours), price));
            } else {
                text2.setText(MyHistoryDetail.this.getString(
                        R.string.booking_details_service_summary,
                        String.valueOf(hours), String.valueOf(minutes), price));
            }
        }

        Animation animation = AnimationUtils.loadAnimation(MyHistoryDetail.this, R.anim.slide_in_from_right);
        layout2.startAnimation(animation);
        addView.addView(layout2);

    }

    private void animateOut(final int index) {
        for (int i = 1; i < dataArray.length() + 1; i++) {
            final View view = addView.getChildAt(index + i);
            Animation removeAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_out_to_right);
            removeAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }
                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                }
                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            view.startAnimation(removeAnimation);
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.service_block :
                int rotateAnimationInt = SERVICES_EXPANDED ? R.anim.rotate_out : R.anim.rotate_in;
                Animation rotate = AnimationUtils.loadAnimation(this, rotateAnimationInt);
                rotate.setFillAfter(true);

                rotate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        int index = serviceDetailsBlock.indexOfChild(servicesBlock);

                        if (SERVICES_EXPANDED) {
                            animateOut(index);

                        } else {
                            for (int i = 0; i <dataArray.length() ; i++) {

                                try {
                                    JSONObject dataObject = dataArray.getJSONObject(i);
                                    insertView(dataObject,index);
                                    index++;
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        SERVICES_EXPANDED = !SERVICES_EXPANDED;
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                serviceExpandButton.startAnimation(rotate);


                break;
            case R.id.booking_detail_cancel_booking :



                new AlertDialogWrapper.Builder(MyHistoryDetail.this)
                        .setTitle("FabBrigade")
                        .setMessage("ํYou want to cancle this booking")
                        .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                cancleBooking(Utils.getCancleURL(id));
                            }
                        })
                        .setPositiveButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();


                break;

        }


    }

    public void setStatusImage(JSONObject data, TextView statusTextView) throws JSONException {
        int statusInt;
        int colorInt = R.color.booking_status_cancelled_unfulfilled;
        switch (Integer.parseInt(data.getString("status"))) {
            case Utils.STATUS_OPEN:
                statusInt = R.string.booking_status_open;
                colorInt = R.color.booking_status_open;
                break;
            case Utils.STATUS_CONFIRMED:
                statusInt = R.string.booking_status_confirmed;
                colorInt = R.color.booking_status_confirmed_completed;
                break;
            case Utils.STATUS_STARTED:
                statusInt = R.string.booking_status_started;
                colorInt = R.color.booking_status_confirmed_completed;
                break;
            case Utils.STATUS_ENDED:
                statusInt = R.string.booking_status_ended;
                colorInt = R.color.booking_status_confirmed_completed;
                break;
            case Utils.STATUS_CANCELLED_CUSTOMER:
                statusInt = R.string.booking_status_cancelled_customer;
                colorInt = R.color.booking_status_cancelled_unfulfilled;
                break;
            case Utils.STATUS_CANCELLED_BRIGADETTE:
                statusInt = R.string.booking_status_cancelled_brigadette;
                colorInt = R.color.booking_status_cancelled_unfulfilled;
                break;
            case Utils.STATUS_NOMATCH:
                statusInt = R.string.booking_status_nomatch;
                colorInt = R.color.booking_status_cancelled_unfulfilled;
                break;
            default:
                statusInt = R.string.booking_status_unknown;
        }
        statusTextView.setText(getString(statusInt));

        Drawable drawable = statusTextView.getBackground();
        if (drawable instanceof ShapeDrawable) {
            ((ShapeDrawable)drawable).getPaint().setColor(getResources().getColor(colorInt));
        } else if (drawable instanceof GradientDrawable) {
            ((GradientDrawable)drawable).setColor(getResources().getColor(colorInt));
        }
    }



    public  void cancleBooking(final String url){
        progress.setVisibility(View.VISIBLE);

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                OkHttpClient okHttpClient = new OkHttpClient();
                String data = Utils.OKHttpPostData(url);
                return data;
            }

            @Override
            protected void onPostExecute(String string) {
                super.onPostExecute(string);
                try {progress.setVisibility(View.GONE);
                    layoutView.setVisibility(View.VISIBLE);

                    JSONObject jsonObject  = new JSONObject(string);
                    if(jsonObject.getString("status").equalsIgnoreCase("200")){


                        Toast.makeText(MyHistoryDetail.this," Booking cancle complete",Toast.LENGTH_LONG).show();

                        getData(Utils.getHistoryDetail(id));

//                        itemObject = jsonObject.getJSONObject("item");
//                        setData(itemObject);

                    }else{


                        return;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }


            }
        }.execute();


    }


}

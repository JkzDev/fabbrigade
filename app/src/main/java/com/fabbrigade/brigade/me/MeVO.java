package com.fabbrigade.brigade.me;

/**
 * Created by Jakki3z on 12/16/15 AD.
 */
public class MeVO {
    String id;
    String address;
    String  booking_dt;



    public  MeVO(String id,String address,String booking_dt){
        super();
        this.id = id;
        this.address = address;
        this.booking_dt = booking_dt;
    }

    public String getBooking_dt() {
        return booking_dt;
    }

    public void setBooking_dt(String booking_dt) {
        this.booking_dt = booking_dt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }





}

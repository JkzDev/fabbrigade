package com.fabbrigade.brigade.me;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fabbrigade.brigade.R;

import java.util.ArrayList;

/**
 * Created by Jakki3z on 12/16/15 AD.
 */
public class MeAdapter extends BaseAdapter {

    private Activity activity;
    ArrayList<MeVO> data = new ArrayList<MeVO>();
    private LayoutInflater inflater = null;
    MeVO me;

    public MeAdapter(Activity a, ArrayList<MeVO> arr) {

        this.activity = a;
        this.data = arr;
        this.inflater = (LayoutInflater) activity.getSystemService(this.activity.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        me = data.get(position);

        if(convertView ==null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item_bookings, null);

            holder.bookings_title= (TextView) convertView.findViewById(R.id.bookings_title);
            holder.bookings_time= (TextView) convertView.findViewById(R.id.bookings_time);

            convertView.setTag(holder);

        }else{
            holder = (ViewHolder)convertView.getTag();

        }

        holder.bookings_title.setText(me.getAddress());
        holder.bookings_time.setText(me.getBooking_dt());




        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView bookings_title;
        TextView bookings_time;

    }




}

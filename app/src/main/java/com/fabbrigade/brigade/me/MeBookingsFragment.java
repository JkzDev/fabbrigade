package com.fabbrigade.brigade.me;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.bookingdetails.BookingDetailsActivity;
import com.fabbrigade.brigade.service.model.Service;
import com.fabbrigade.brigade.shared.BaseFragment;
import com.fabbrigade.brigade.shared.QueryUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by Laptop on 10/25/2015.
 */
public class MeBookingsFragment extends BaseFragment {

    protected static final int MODE_UPCOMING = 0;
    protected static final int MODE_PAST = 1;
    protected static final String ARG_MODE = "mode";

    private int mode;

    private List<ParseObject> pinnedObjects;

    @Bind(R.id.booking_list) RecyclerView recyclerView;
    @Bind(R.id.booking_list_empty) TextView recyclerViewEmptyMessage;
    @Bind(R.id.booking_list_loading) ProgressBar recyclerViewLoadingIndicator;

    private OnBookingClickListener mBookingClickListener = new OnBookingClickListener() {
        @Override
        public void onBookingClick(Booking booking) {
            Intent i = new Intent(getActivity(), BookingDetailsActivity.class);

            i.putExtra(BookingDetailsActivity.ARG_BOOKING_ID, booking.getObjectId());

            startActivity(i);
        }
    };

    public static String getTitle(int mode, Context ctx) {
        switch (mode) {
            case MODE_UPCOMING:
                return ctx.getString(R.string.tab_upcoming);
            case MODE_PAST:
                return ctx.getString(R.string.tab_past);
            default:
                return "";
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            ParseObject.unpinAll(pinnedObjects);
        } catch (ParseException e) {
            Timber.e(e, "Failed to unpin objects");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recyclerview, container, false);
        ButterKnife.bind(this, rootView);
        mode = getArguments().getInt(ARG_MODE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ParseQuery<ParseObject> query = ParseQuery.getQuery("Booking")
                .include("services")
                .include("brigadette")
                .include("services.category.parent")
                .include("rating");

        QueryUtils.addMatchCurrentUserToQuery(query, false);

        switch (mode) {
            case MODE_PAST:
                query.whereLessThan("date", Calendar.getInstance().getTime())
                        .orderByDescending("date");
                break;
            case MODE_UPCOMING:
                query.whereGreaterThan("date", Calendar.getInstance().getTime())
                        .orderByAscending("date");
                break;
        }
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> objects, ParseException e) {
                if (e != null) {
                    Timber.e(e, "Error fetching bookings."); //TODO display error
                    return;
                }

                ParseObject.pinAllInBackground(objects);
                pinnedObjects = objects;

                recyclerViewLoadingIndicator.setVisibility(View.GONE);
                if (objects.size() == 0) {
                    recyclerViewEmptyMessage.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setAdapter(new BookingsAdapter(objects, mBookingClickListener));
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                }

            }
        });
    }

    private class BookingsAdapter extends RecyclerView.Adapter<BookingViewHolder> {

        private List<ParseObject> mBookings;
        private OnBookingClickListener mListener;

        public BookingsAdapter(List<ParseObject> bookings, OnBookingClickListener listener) {
            mBookings = bookings;
            mListener = listener;
        }

        @Override
        public BookingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_bookings, parent, false);

            return new BookingViewHolder(view);
        }

        @Override
        public void onBindViewHolder(BookingViewHolder holder, int position) {

            try {


            final Booking booking = (Booking)mBookings.get(position);
            List<Service> services = booking.getServices();
            String name = services.get(0).getCategory().getParent().getName();
            holder.title.setText(name);
            Date date = booking.getDate();
            booking.getServices();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, MMMM d, h:mm a", Locale.getDefault()); //TODO need length
            holder.time.setText(simpleDateFormat.format(date));

            if (mode == MODE_UPCOMING) {
                holder.statusBlock.setVisibility(View.VISIBLE);
                booking.setStatusImage(getActivity(), holder.status);
            }

            holder.bookingBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onBookingClick(booking);
                }
            });

//            if (category.isEnabled()) { TODO
//                holder.itemView.setClickable(true);
//                holder.book.setVisibility(View.VISIBLE);
//                holder.book.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mListener.onServiceClick(category.getObjectId());
//                    }
//                });
//                holder.itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mListener.onServiceClick(category.getObjectId());
//                    }
//                });
//            } else {
//                holder.itemView.setClickable(false);
//                holder.book.setVisibility(View.GONE);
//            }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return mBookings == null ? 0 : mBookings.size();
        }
    }

    protected class BookingViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView time;
        TextView status;
        LinearLayout statusBlock;
        RelativeLayout bookingBlock;

        public BookingViewHolder(View view) {
            super(view);
//            ButterKnife.bind(this, view);
            title  = (TextView)view.findViewById(R.id.bookings_title);
            time  = (TextView)view.findViewById(R.id.bookings_time);
            status  = (TextView)view.findViewById(R.id.bookings_status);
            statusBlock  = (LinearLayout)view.findViewById(R.id.bookings_status_block);
            bookingBlock  = (RelativeLayout)view.findViewById(R.id.booking_block);


        }
    }

    protected interface OnBookingClickListener {
        void onBookingClick(Booking booking);
    }
}

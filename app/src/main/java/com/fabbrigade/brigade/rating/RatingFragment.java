package com.fabbrigade.brigade.rating;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.fabbrigade.brigade.R;
import com.fabbrigade.brigade.booking.model.Booking;
import com.fabbrigade.brigade.rating.model.Rating;
import com.fabbrigade.brigade.service.model.Service;
import com.fabbrigade.brigade.shared.model.FabUser;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import icepick.Icepick;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import timber.log.Timber;


/**
 * Created by Laptop on 10/20/2015.
 */
public class RatingFragment extends DialogFragment implements View.OnClickListener, RatingBar.OnRatingBarChangeListener,
        TextWatcher {

    @Bind(R.id.rate_service_description) TextView rateServiceDescription;
    @Bind(R.id.booking_details_rate_bar) RatingBar rateBar;
    @Bind(R.id.rate_review) EditText reviewText;
    @Bind(R.id.rate_finish) Button finishButton;
    @Bind(R.id.profile_image) ImageView profileImage;
    public static final String ARG_BOOKING_ID = "booking";
    private static final String ARG_RATING_ID = "rating_id";

    private Rating bridgadetteRating;
    private Booking booking;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rate, container, false);
        ButterKnife.bind(this, rootView);
        finishButton.setOnClickListener(this);
        rateBar.setOnRatingBarChangeListener(this);
        reviewText.addTextChangedListener(this);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();

        final String bookingId = bundle.getString(ARG_BOOKING_ID);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Booking");

        query.fromLocalDatastore();
        query.getInBackground(bookingId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e != null) {
                    Timber.e(e, "Failed to get Booking");
                    getDialog().dismiss();
                    return;
                }
                booking = (Booking) object;
                displayBookingInfo();
            }});

    }
    private void displayBookingInfo() {
        ArrayList<Service> services = (ArrayList<Service>) booking.getServices();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d, yyyy", Locale.getDefault());
        String serviceDescription = Service.getServiceProvidedString(getActivity(), services);

        FabUser brigadette = booking.getBrigadette();

        if (brigadette == null || brigadette.getFacebookId() == null) {
            Timber.e("Brigadette is null");
            Crashlytics.log(booking.getObjectId() + " has null brigadette in review status");
            getDialog().dismiss();
        }

        String id = brigadette.getFacebookId();

        if (id != null) {
            String img_value = "http://graph.facebook.com/" + id + "/picture?type=normal";
            Glide.with(this)
                    .load(img_value)
                    .dontAnimate()
                    .bitmapTransform(new CropCircleTransformation(getActivity()))
                    .into(profileImage);
        }

        enableFinishButton(false);
        rateServiceDescription.setText(getString(R.string.text_rate_service_description, brigadette.getFirstName(getActivity()),
                serviceDescription, simpleDateFormat.format(booking.getDate())));
        bridgadetteRating = ParseObject.createWithoutData(Rating.class, getArguments().getString(ARG_RATING_ID));
    }

    public static RatingFragment newInstance(Booking booking) {
        RatingFragment fragment = new RatingFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_BOOKING_ID, booking.getObjectId());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onClick(View v) {
        booking.setRating(bridgadetteRating);
        booking.saveInBackground();
        booking.unpinInBackground();
        dismiss();
    }


    public void enableFinishButton(boolean enable) {
        finishButton.setEnabled(enable);
        int color = (enable) ? R.color.orange : R.color.orange_light;
        finishButton.setBackgroundColor(getResources().getColor(color));
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        if (rating == 0.0) {
            enableFinishButton(false);
        } else {
            enableFinishButton(true);
        }
        bridgadetteRating.setStars((int) rating);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        bridgadetteRating.setComments(s.toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }
}

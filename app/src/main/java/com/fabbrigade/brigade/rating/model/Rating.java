package com.fabbrigade.brigade.rating.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Copyright FabBrigade 2015
 */

@ParseClassName("Rating")
public class Rating extends ParseObject {
    private static final String COLUMN_COMMENTS = "comments";
    private static final String COLUMN_STARS = "stars";

    public int getStarsInt() {
        Number stars = getNumber(COLUMN_STARS);
        return stars.intValue();
    }

    public void setStars(int stars) {
        if (stars != 0) {
            put(COLUMN_STARS, stars);
        }
    }

    public void setComments(String comments) {
        if (comments != null) {
            put(COLUMN_COMMENTS, comments);
        }
    }
}

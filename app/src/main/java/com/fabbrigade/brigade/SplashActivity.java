package com.fabbrigade.brigade;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.fabbrigade.brigade.category.MainActivity;

/**
 * Created by kyles on 11/29/2015.
 */
public class SplashActivity extends AppCompatActivity{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        Intent intent = new Intent(SplashActivity.this, MainActivity.class);


        startActivity(intent);
        finish();
    }
}

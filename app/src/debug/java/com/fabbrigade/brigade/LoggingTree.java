package com.fabbrigade.brigade;

import android.content.Context;

import timber.log.Timber;

/**
 * Copyright FabBrigade 2015
 */
public class LoggingTree extends Timber.DebugTree {
    public LoggingTree(Context context) {
        // no-op
    }
}

package com.fabbrigade.brigade;

import com.facebook.stetho.Stetho;
import com.parse.Parse;
import com.parse.interceptors.ParseStethoInterceptor;

/**
 * Copyright FabBrigade 2015
 */
public class FabBrigadeApplication extends BaseFabBrigadeApplication {
    @Override
    public void onCreate() {
        Stetho.initializeWithDefaults(this);
        Parse.addParseNetworkInterceptor(new ParseStethoInterceptor());
        super.onCreate();
    }
}

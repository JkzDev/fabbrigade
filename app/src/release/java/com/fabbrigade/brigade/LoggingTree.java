package com.fabbrigade.brigade;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Copyright FabBrigade 2015
 */
public class LoggingTree extends Timber.Tree {

    public LoggingTree(Context context) {
        Fabric.with(context.getApplicationContext(), new Crashlytics());
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (priority == Log.DEBUG || priority == Log.VERBOSE) {
            return;
        }

        Crashlytics.log(priority, tag, message);
        if (t != null) {
            Crashlytics.logException(t);
        }
    }
}
